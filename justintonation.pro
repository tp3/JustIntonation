#==============================================================================
#                  Just Intonation - Application Project File
#==============================================================================
#
# To compile Android on Mint/Ubuntu install gcc-arm-linux-androidabi
#
# QT-SPECIFIC ISSUES
#
# To enable fast compilation in Qt insert the following environment variable:
# Tools > Options > Build & Run > Kits > (pick your toolchain) -> Environment-> Change...
# MAKEFLAGS=-j8
#
#
# PLATFORM-SPECIFIC ISSUES
#
# MACOSX/IOS:
# In case of a  "sysroot not found" error (wrong XCode library) the reason might be
# a hidden .qmake.stash or -qmake.cache file in the build directory (use locate)
#
# ANDROID:
# If Android tries to deploy with the SDK build tools of an old version
# check the file gradle.properties and make sure that it is excluded in .gitignore

TEMPLATE = subdirs
CONFIG += ordered

CONFIG+=suppress_vcproj_warnings
SUBDIRS += \
    application \
