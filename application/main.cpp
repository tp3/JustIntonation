/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//                              JustIntonation
//                          Main application file
//=============================================================================

#include <QApplication>
#include <QQmlApplicationEngine>
#include <QLoggingCategory>

#include "config.h"
#include "system/runguard.h"
#include "system/translator.h"
#include "system/downloader.h"
#include "system/systemtools.h"
#include "system/filehandler.h"
#include "application.h"


int main(int argc, char *argv[])
{
    // Assign Main thread name for debugging
    SystemTools::setThreadName("MainThread");

    // Suppress warning "qt.network.ssl: Error receiving trust for a CA certificate"
    // This warning is caused by the assumption that the download page is https,
    // but actually it is non-encrypted.
    QLoggingCategory::setFilterRules(QStringLiteral("qt.network.ssl=false"));

    // Create the application
    QApplication app(argc, argv);

    // If another instance is already running show a message box and quit:
    #if defined(INT_INCLUDE_RUNGUARD)
    RunGuard runguard (INT_APPLICATIONNAME);
    if (runguard.anotherInstanceIsRunning()) return 0;
    #endif

    // Register map key->cents for tuning corrections
    qRegisterMetaType<QMap<int,double>>();

    // Register translator
    qmlRegisterType<Translator> ("JustIntonation",1,0,"Translator");

    // Specify unique identifiers, needed for QSettings
    app.setOrganizationName(INT_ORGANIZATIONNAME);
    app.setOrganizationDomain(INT_ORGANIZATIONDOMAIN);
    app.setApplicationName(INT_APPLICATIONNAME);

    // Create, load and init the Qml application engine
    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/Main.qml")));
    if (engine.rootObjects().isEmpty())
    {
        qDebug() << "ERROR: main.cpp: Could not load Qml objects, exiting.\n";
        return -1;
    }

    // Create an instance of the Application class
    Application justintonation(app);

    // Initialize translator
    Translator::init(app,engine,INT_APPLICATIONNAME_LOWERCASE,":/languages/translations");

    // execute the application
    justintonation.init(engine);
    justintonation.start();
    app.exec();

    // shut down and exit
    justintonation.stop();
    justintonation.exit();
}
