/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//                             Application Class
//=============================================================================

#include "application.h"

#include <QLibraryInfo>
#include <QDir>
#include <QFile>
#include <QStandardPaths>
#include <QResource>
#include <QQmlProperty>
#include <QDialogButtonBox>
#include <QVariant>

#include "config.h"

// TODO Global application pointer for debugging, can be removed later
Application *globalApplicationPointer = nullptr;

//-----------------------------------------------------------------------------
//                          Application Constructor
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Application constructor
///////////////////////////////////////////////////////////////////////////////

Application::Application(QApplication &app)
    : pApplication(&app)
    , mStarted(false)
    , mSettings()
    , mLocale()

    , mDownloader(INT_SAMPLES_REPOSITORY)
    , mFileHandler()
    , mMidiPlayer()
    , mMidi()
    , mMidiMicrotonalConverter()
    , mAudioOutput()
    , mNumberOfExample(1)
    , mInstrument()
    , mSoundGenerator()
    , mMidiHandler(mSoundGenerator,mAudioOutput)
    , mTuner()
    , mTuningLog()
    , mKeyboard()
    , mAvailableMidiDevices()
    , mAvailableMidiOutputDevices()
    , mCurrentMidiDevice("")
    , mMidiAutomaticRecognition(true)
    , mMidiPlayerWasPlaying(false)
    , mSuspended(false)
    , mLastEmittedPitchProgression(0)
{
    setModuleName("Main");

    //    this->setVerbosity(4); // Application
    //    mSoundGenerator.setVerbosity(4);
    //    mMidiHandler.setVerbosity(4);
    //    mMidiPlayer.setVerbosity(4);
    //    mTuner.setVerbosity(4);
    //    mKeyboard.setVerbosity(4);
    //    mDownloader.setVerbosity(4);
    //    mInstrument.setVerbosity(4);
    //    mMidi.setVerbosity(4);
    //    mMidiMicrotonalConverter.setVerbosity(4);
    //    mAudioOutput.setVerbosity(4);
}


//-----------------------------------------------------------------------------
//                    Initialization of the application
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Initialization of the application
/// \details This function initializes the application but it does not yet
/// start the components. Moreover, the application is "wired" from here:
/// For easy reference all signal-slot connections can be found here.
/// \param engine : Reference to the QML application engine which handels the GUI
///////////////////////////////////////////////////////////////////////////////

void Application::init (QQmlApplicationEngine &engine)
{
    disableScreenSaver();

    //======================= C++ INTERNAL CONNECTIONS ========================

    // Connect application status
    connect(pApplication,&QApplication::applicationStateChanged,this,&Application::handleApplicationStates);

    // Downloading instruments from the server to local disk
    connect(&mDownloader,SIGNAL(signalNewFileDownloaded(QString)),this,SLOT(newInstrumentDownloaded(QString)));
    connect(&mDownloader,SIGNAL(signalAllFilesDownloaded()),this,SLOT(allInstrumentsDownloaded()));

    // Instrument loading from local disk into RAM
    connect(this,&Application::startLoadingInstrument,&mInstrument,&Instrument::loadInstrument);
    connect(&mInstrument,SIGNAL(signalLoadingFinished(bool)),this,SLOT(loadingFinished(bool)));
    connect(mInstrument.getVoice(),SIGNAL(signalArtificialSoundGenerationComplete(bool)),this,SLOT(loadingFinished(bool)));

    // Connect Midi sources to synthesizer and tuner
    connect(&mMidi,&Midi::receivedMidiMessage, &mMidiHandler,&MidiHandler::receiveMidiEvent);
    connect(&mMidiPlayer,&MidiPlayer::signalMidiEvent, &mMidiHandler,&MidiHandler::receiveMidiEvent);
    connect(&mKeyboard,&TouchscreenKeyboard::sendTouchpadMidiEvent, &mMidiHandler,&MidiHandler::receiveMidiEvent);
    connect(&mMidi,&Midi::receivedMidiMessage, &mTuner,&Tuner::receiveMidiEvent);
    connect(&mMidiPlayer,&MidiPlayer::signalMidiEvent, &mTuner,&Tuner::receiveMidiEvent);
    connect(&mKeyboard,&TouchscreenKeyboard::sendTouchpadMidiEvent, &mTuner,&Tuner::receiveMidiEvent);

    // Tuning log
    connect(&mMidiPlayer,&MidiPlayer::signalMidiEvent,&mTuningLog,&LogFile::receiveMidiEvent);
    connect(&mTuner,&Tuner::signalTuningCorrections,&mTuningLog,&LogFile::receiveTuningCorrections);
    connect(&mMidi,&Midi::receivedMidiMessage, &mTuningLog,&LogFile::receiveMidiEvent);

    // Connect microtonal Midi converter
    connect(&mMidi,&Midi::receivedMidiMessage, &mMidiMicrotonalConverter,&MidiMicrotonal::receiveMidiEvent);
    connect(&mMidiPlayer,&MidiPlayer::signalMidiEvent, &mMidiMicrotonalConverter,&MidiMicrotonal::receiveMidiEvent);
    connect(&mKeyboard,&TouchscreenKeyboard::sendTouchpadMidiEvent,&mMidiMicrotonalConverter,&MidiMicrotonal::receiveMidiEvent);
    connect(&mMidiMicrotonalConverter,&MidiMicrotonal::sendMidiEvent,&mMidi,&Midi::sendMidiMessage);
    connect(&mMidi,&Midi::onStatusChanged,
            &mMidiMicrotonalConverter,&MidiMicrotonal::reInitializeConvertedMidiStream);

    // Connect pedal signals with sound-generating system
    connect(&mMidiHandler,&MidiHandler::signalSustainPedal,&mSoundGenerator,&SoundGenerator::setSustainPedal);
    connect(&mMidiHandler,&MidiHandler::signalSostenutoPedal,&mSoundGenerator,&SoundGenerator::setSostenutoPedal);
    connect(&mMidiHandler,&MidiHandler::signalSoftPedal,&mSoundGenerator,&SoundGenerator::setSoftPedal);

    // Connect tuner output with the sound-generating system and microtonal converter
    connect(&mTuner,&Tuner::signalTuningCorrections, &mSoundGenerator,&SoundGenerator::receiveTuningCorrections);
    connect(&mTuner,&Tuner::signalTuningCorrections, &mMidiMicrotonalConverter,&MidiMicrotonal::receiveTuningCorrections);

    // Audio
    connect(&mAudioOutput,SIGNAL(onAudioDevicesAdded(QStringList)),this,SLOT(onAudioDevicesAdded(QStringList)));
    connect(&mAudioOutput,SIGNAL(onAudioDevicesRemoved(QStringList)),this,SLOT(onAudioDevicesRemoved(QStringList)));
    connect(&mAudioOutput,SIGNAL(onChangeOfAvailableAudioDevices(QStringList)),this,SLOT(onChangeOfAvailableAudioDevices(QStringList)));
    connect(&mAudioOutput,SIGNAL(onCurrentDeviceParametersChanged()),this,SLOT(onCurrentDeviceParametersChanged()));
    connect(&mAudioOutput,SIGNAL(onConnectionSuccessfullyEstablished(bool)),this,SLOT(onConnectionSuccessfullyEstablished(bool)));
    connect(this,SIGNAL(connectAudioDevice(bool)),&mAudioOutput,SLOT(connectDevice(bool)));

    //================ CONNECTIONS BETWEEN C++ AND THE QML LAYER ==============

    if (engine.rootObjects().empty()) return;
    QObject* pQml = engine.rootObjects().first();
    if (not pQml) return;

    // Instrument selector
    connect(this,SIGNAL(setQmlInstrumentNames(QVariant)),pQml,SLOT(setQmlInstrumentNames(QVariant)));
    connect(this,SIGNAL(setQmlSelectedInstrument(QVariant)),pQml,SLOT(setQmlSelectedInstrument(QVariant)));
    connect(pQml,SIGNAL(selectInstrument(int)),this,SLOT(loadInstrument(int)));
    connect(pQml,SIGNAL(updateInstruments()),this,SLOT(updateInstruments()));

    // Progress bar
    connect(&mInstrument,SIGNAL(showProgressBar(QVariant)),pQml,SLOT(showProgressBar(QVariant)));
    connect(&mInstrument,SIGNAL(hideProgressBar()),pQml,SLOT(hideProgressBar()));
    connect(mInstrument.getVoice(),SIGNAL(setProgressText(QVariant)),pQml,SLOT(setProgressText(QVariant)));
    connect(mInstrument.getVoice(),SIGNAL(setProgress(QVariant)),pQml,SLOT(setProgress(QVariant)));
    connect(pQml,SIGNAL(progressCancelButton()),this,SLOT(cancelLoading()));

    // Midi player
    connect(&mMidiPlayer,SIGNAL(signalPlayingStatusChanged(QVariant)),pQml,SLOT(setPlayingStatus(QVariant)));
    connect(pQml,SIGNAL(togglePlayPause()),&mMidiPlayer,SLOT(togglePlayPause()));
    connect(pQml,SIGNAL(hearExample(int)),this,SLOT(hearExample(int)));
    connect(this,SIGNAL(setSelectedExample(QVariant)),pQml,SLOT(setQmlSelectedExample(QVariant)));
    connect(pQml,SIGNAL(rewind()),&mMidiPlayer,SLOT(rewind()));
    connect(pQml,SIGNAL(progressChangedManually(double)),&mMidiPlayer,SLOT(setMidiProgress(double)));
    connect(pQml,SIGNAL(setTempoFactor(double)),&mMidiPlayer,SLOT(setTempo(double)));
    connect(pQml,SIGNAL(setRepeatMode(bool)),&mMidiPlayer,SLOT(setRepeatMode(bool)));
    connect(pQml,SIGNAL(openFile(QString)),&mMidiPlayer,SLOT(loadUrl(QString)));
    connect(&mFileHandler,&FileHandler::openMidiFile,&mMidiPlayer,&MidiPlayer::loadFile);
    connect(&mMidiPlayer,SIGNAL(signalProgressInPercent(QVariant)),pQml,SLOT(setMidiProgress(QVariant)));
    connect(&mMidiPlayer,SIGNAL(setDisplayedMidiFilename(QVariant)),pQml,SLOT(setDisplayedMidiFilename(QVariant)));

    // Temperament selection
    connect(pQml,SIGNAL(signalTuningMode(int,int)),&mTuner,SLOT(setTuningMode(int,int)));
    connect(pQml,SIGNAL(signalCentsChanged(int,double)),&mTuner,SLOT(setIntervalSize(int,double)));
    connect(pQml,SIGNAL(signalWeightsChanged(int,double)),&mTuner,SLOT(setIntervalWeight(int,double)));
    connect(&mTuner,SIGNAL(signalReadyForReceivingParameters()),pQml,SLOT(sendAllCentsAndWeights()));
    connect(pQml,SIGNAL(signalLoadCustomTemperament()),&mFileHandler,SLOT(loadData()));
    connect(pQml,SIGNAL(signalSaveCustomTemperament(QString,QString,QString)),&mFileHandler,SLOT(saveData(QString,QString,QString)));
    connect(&mFileHandler,SIGNAL(loadedData(QVariant,QVariant,QVariant)),pQml,SLOT(loadCustomTemperament(QVariant,QVariant,QVariant)));

    // Tuning delay
    connect (pQml,SIGNAL(setTuningDelay(double)),&mTuner,SLOT(setDelayParameter(double)));

    // Static tuning
    connect(pQml,SIGNAL(setStaticTuning(bool,int)),&mTuner,SLOT(setStaticTuningMode(bool,int)));

    // Drift correction
    connect (pQml,SIGNAL(setDriftCorrectionParameter(double)),&mTuner,SLOT(setPitchProgressionCompensationParameter(double)));
    connect (pQml,SIGNAL(setMemoryLength(double)),&mTuner,SLOT(setMemoryLength(double)));
    connect (pQml,SIGNAL(resetPitchProgression()),&mTuner,SLOT(resetPitchProgression()));

    // Tuning log
    connect (&mTuningLog,SIGNAL(signalNewLogMessage(QVariant)),pQml,SLOT(appendLogfile(QVariant)));
    connect (&mMidiPlayer,SIGNAL(signalPlayingStatusChanged(QVariant)),pQml,SLOT(clearLogFile(QVariant)));
    connect (pQml,SIGNAL(activateLogFile(bool)),&mTuningLog,SLOT(activateLogFile(bool)));

    // Target frequency
    connect (&mMidiHandler,SIGNAL(changeTargetFrequency(QVariant)),pQml,SLOT(changeTargetFrequency(QVariant)));
    connect (pQml,SIGNAL(setFrequencyOfA4(double)),&mTuner,SLOT(setFrequencyOfA4(double)));

    // Circular gauge for pitch progression
    connect (&mTuner,SIGNAL(signalAveragePitchDrift(double)),this,SLOT(emitAveragePitchProgression(double)));
    connect (this,SIGNAL(signalCircularGauge(QVariant)),pQml,SLOT(setAveragePitchDeviation(QVariant)));
    connect (&mTuner,SIGNAL(signalIntervalString(QVariant)),pQml,SLOT(setIntervalString(QVariant)));
    connect (&mTuner,SIGNAL(signalTension(QVariant)),pQml,SLOT(setTension(QVariant)));

    // Downloading instruments from the internet
    connect(pQml,SIGNAL(startDownload(bool)),this,SLOT(startDownload(bool)));
    connect(&mDownloader,SIGNAL(signalDownloading(QVariant)),pQml,SLOT(showDownloader(QVariant)));
    connect(&mDownloader,SIGNAL(signalProgress(QVariant,QVariant)),pQml,SLOT(setDownloaderProgress(QVariant,QVariant)));
    connect(&mDownloader,SIGNAL(signalNoInternetConnection(QVariant)),pQml,SLOT(noInternetConnection(QVariant)));

    // Audio settings
    connect(this,SIGNAL(setAudioDeviceName(QVariant)),pQml,SLOT(setAudioDeviceName(QVariant)));
    connect(this,SIGNAL(setAudioDeviceList(QVariant,QVariant)),pQml,SLOT(setAudioDeviceList(QVariant,QVariant)));
    connect(this,SIGNAL(setAudioSampleRates(QVariant,QVariant)),pQml,SLOT(setAudioSampleRates(QVariant,QVariant)));
    connect(pQml,SIGNAL(selectAudioParameters(int,int,int,int)),this,SLOT(selectAudioParameters(int,int,int,int)));
    connect(pQml,SIGNAL(resetAudioParameters()),this,SLOT(resetAudioParameters()));
    connect(this,SIGNAL(setAudioBufferSizes(QVariant,QVariant)),pQml,SLOT(setBufferSizes(QVariant,QVariant)));

    // Touchscreen keyboard
    connect(pQml,SIGNAL(sendTouchscreenKeyboardEvent(QVariant)),
            &mKeyboard,SLOT(receiveTouchpadKeyboardEvent(QVariant)));
    connect(pQml,SIGNAL(setToggleMode(bool)),
            &mKeyboard,SLOT(setToggleMode(bool)));
    connect(pQml,SIGNAL(clearTouchscreenKeyboard()),
            &mKeyboard,SLOT(clear()));
    connect(&mKeyboard,SIGNAL(highlightKey(QVariant,QVariant)),
            pQml,SLOT(highlightKey(QVariant,QVariant)));

    // Initialization procedure
    connect(this,SIGNAL(getSettingsFromQml()),pQml,SLOT(getSettingsFromQml()));

    // TODO Message for debugging on mobile devices (temporarily)
    globalApplicationPointer = this;
    connect(this,SIGNAL(sendMobileMessage(QVariant)),pQml,SLOT(getMobileErrorMessage(QVariant)));

    // Midi remapper
    connect(pQml,SIGNAL(onMidiOutputChannelChanged(int)),&mMidiMicrotonalConverter,SLOT(setMidiOutputChannel(int)));

    // Open Midi file dialog
    connect(pQml,SIGNAL(openFileOpenDialog(int,int)),&mFileHandler,SLOT(openMidiFileDialog(int,int)));

    //======================= MAIN INITIALIZATION =============================

    PlatformTools::getSingleton().init(*this);

    mAudioOutput.init();
    mAudioOutput.setDefaultBufferSize(INT_BUFFERSIZE);

    int buffersize=mSettings.value("audiooutput/buffersize",INT_BUFFERSIZE).toInt();
    int packetsize=mSettings.value("audiooutput/packetsize",INT_PACKETSIZE).toInt();

    emit setAudioBufferSizes (buffersize,packetsize);
    mInstrument.init();
    mSoundGenerator.init(this,mInstrument.getVoice());
    mSoundGenerator.setMaxPacketSize(packetsize);
    mAudioOutput.setAudioGenerator(mSoundGenerator.getSampler());
    mInstrument.init();
    mMidiHandler.init();

    mTuner.init();
    mMidi.init(pQml);
    mMidiMicrotonalConverter.init();
    mKeyboard.init();
    mMidiPlayer.init();
}


//-----------------------------------------------------------------------------
//                           Start the application
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Start the application and its components
///////////////////////////////////////////////////////////////////////////////

void Application::start()
{
    if (mStarted) return;
    mStarted = true;
    mAudioOutput.start();
    mInstrument.start();
    setInstrumentSelector();
    mSoundGenerator.start();
    mMidiHandler.start();

    mTuner.start();
    mMidiMicrotonalConverter.start();
    mKeyboard.start();
    mMidiPlayer.start();

    // Load Midi file
    bool midiFileLoaded = false;
    #if (!defined(Q_OS_IOS) && !defined(Q_OS_ANDROID))
    if (pApplication->arguments().size()==2)
        midiFileLoaded = loadMidiFile(QUrl(pApplication->arguments()[1]).path());
    #endif
    if (not midiFileLoaded) mMidiPlayer.loadFile(":/audio/example1.mid");

    getSettingsFromQml();
}


//-----------------------------------------------------------------------------
//                            Stop the application
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Stop the application and its components
///////////////////////////////////////////////////////////////////////////////

void Application::stop()
{
    if (not mStarted) return;
    mStarted = false;

    mMidiPlayer.stop();
    mKeyboard.stop();
    mMidiMicrotonalConverter.stop();
    mTuner.stop();


    QThread::msleep(200); // Wait 200 msec for the device to detach
    mMidiHandler.stop();
    mSoundGenerator.stop();
    mInstrument.stop();
    mAudioOutput.stop();
    mDownloader.stop();
}


//-----------------------------------------------------------------------------
//                                    Exit
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Exit from the application and all its components
///////////////////////////////////////////////////////////////////////////////

void Application::exit()
{
    mMidiPlayer.exit();
    mKeyboard.exit();
    mMidiMicrotonalConverter.exit();
    mTuner.exit();

    mMidiHandler.exit();
    mSoundGenerator.exit();
    mInstrument.exit();
    mAudioOutput.exit();
}


//-----------------------------------------------------------------------------
//                                    Suspend
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Suspend the application
/// This function suspends the application and all modules. This is essential
/// e.g. on mobile devices, when the application is sent to the background
///////////////////////////////////////////////////////////////////////////////

void Application::suspend()
{
    if (mSuspended) return;
    LOGMESSAGE << "Suspend application";

    mMidiPlayer.suspend();
    mKeyboard.suspend();
    mMidiMicrotonalConverter.suspend();
    mTuner.suspend();
    mAudioOutput.suspend();
    mInstrument.suspend();
    mSoundGenerator.suspend();
    mMidiHandler.suspend();

    mSuspended = true;
}


//-----------------------------------------------------------------------------
//                                    Resume
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief resume
///////////////////////////////////////////////////////////////////////////////

void Application::resume()
{
    if (not mSuspended) return;
    LOGMESSAGE << "Resume application";

    mMidiHandler.resume();
    mSoundGenerator.resume();
    mInstrument.resume();
    mAudioOutput.resume();
    mTuner.resume();
    mMidiMicrotonalConverter.resume();
    mKeyboard.resume();
    mMidiPlayer.resume();

    mSuspended = false;
}


//-----------------------------------------------------------------------------
//             Function executed when application state changes
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Function executed when application state changes (platform-dependent)
/// Takes action if the state of the application changes. Needed for Android.
///////////////////////////////////////////////////////////////////////////////

void Application::handleApplicationStates(Qt::ApplicationState state)
{
//#if defined(Q_OS_ANDROID) || defined(Q_OS_IOS)
//    LOGMESSAGE << "******** Application state changed to" << state;
//    if (state==Qt::ApplicationActive) resume();
//    else suspend();
//#else
//    (void)state;
////    if (state==Qt::ApplicationActive) resume();
////    else suspend();
//#endif

    LOGMESSAGE << "******** Application state changed to" << state;
    if (state==Qt::ApplicationSuspended) suspend();
    else if (state==Qt::ApplicationActive) resume();

}


//-----------------------------------------------------------------------------
//                            Start download
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Private slot: Start download
///////////////////////////////////////////////////////////////////////////////

void Application::startDownload (bool forced)
{
    LOGMESSAGE << "Slot called from Qml: Start download, forced =" << forced;
    mDownloader.stop();
    mDownloader.start(forced);
}


//-----------------------------------------------------------------------------
//                             Hear an example
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Private slot: Hear a Midi example
/// \param number : number of the Midi example to be played
///////////////////////////////////////////////////////////////////////////////

void Application::hearExample(int number)
{
    if (number > 0)
    {
        if (mMidiPlayer.isPlaying()) mMidiPlayer.pause();
        mNumberOfExample = number;
        if (mNumberOfExample > INT_NUMBER_OF_EXAMPLES) mNumberOfExample=1;
        QString resource = ":/audio/example"
                + QString::number(mNumberOfExample)+".mid";
        LOGMESSAGE << "loading " << resource;
        mMidiPlayer.loadFile(resource);
        mMidiPlayer.play();
    }
    else if (mMidiPlayer.isPlaying())
    {
        mMidiPlayer.pause();
        mNumberOfExample++;
        if (mNumberOfExample > INT_NUMBER_OF_EXAMPLES) mNumberOfExample=1;
        setSelectedExample(mNumberOfExample-1);
        QString resource = ":/audio/example"
                + QString::number(mNumberOfExample)+".mid";
        LOGMESSAGE << "loading " << resource;
        mMidiPlayer.loadFile(resource);
    }
    else mMidiPlayer.play();
}


//-----------------------------------------------------------------------------
//                 Emit the newly calculated average pitch
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Emit the newly calculated average pitch progression
/// \details This function restricted the displayed range of the pitch
/// progression according to the range of the circular gauge
/// \param pitchProgression : Value of the pitch progression in cents
///////////////////////////////////////////////////////////////////////////////

void Application::emitAveragePitchProgression (double pitchProgression)
{
    if (pitchProgression > -50 and pitchProgression < 50)
    {
        mLastEmittedPitchProgression = pitchProgression;
        emit signalCircularGauge (mLastEmittedPitchProgression);
    }
}


//-----------------------------------------------------------------------------
//           Update the list of instruments in the Qml Combo box
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Update the list of instruments in the Qml Combo box
/// \return Number of instruments
///////////////////////////////////////////////////////////////////////////////

QStringList Application::updateShownInstrumentNames()
{
    LOGMESSAGE << "Update shown instrument names";
    QStringList instruments = mDownloader.getPathsOfDownloadedFiles();
    LOGSTATUS << "The paths of the instrument files are:" << instruments;

    // Translate the corresponding InstrumentQml names
    QStringList InstrumentQmlNames = instruments;
    for (QString &name : InstrumentQmlNames)
    {
        if (name.contains("Piano")) name=tr("Grand Piano");
        else if (name.contains("Organ")) name=tr("Organ");
        else if (name.contains("Harpsichord")) name=tr("Harpsichord");
        else name=tr("Unknown Instrument");
    }
    InstrumentQmlNames.prepend(tr("Artificial sound"));
    InstrumentQmlNames.append(tr("External device"));
    LOGSTATUS << "Corresponding instrument names names:" << InstrumentQmlNames;

    emit setQmlInstrumentNames (InstrumentQmlNames);
    return InstrumentQmlNames;
}


//-----------------------------------------------------------------------------
//                 Set the content of the instrument selector
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Set the content of the instrument selector
///////////////////////////////////////////////////////////////////////////////

void Application::setInstrumentSelector (int index)
{
    int numberOfInstruments = updateShownInstrumentNames().size();
    if (index==0) index = mSettings.value("controller/instrumentindex",0).toInt();
    if (index >= numberOfInstruments) index = 0;
    emit setQmlSelectedInstrument(index);
    loadInstrument(index);
}


//-----------------------------------------------------------------------------
//                              Load an instrument
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Private slot: Load an instrument
///////////////////////////////////////////////////////////////////////////////

void Application::loadInstrument (int selectedindex)
{
    // Index structure:
    // 0: Artificial sound
    // 1: Instrument 0
    // 2: Instrument 1
    // 3: Instrument 2
    // 4: External Midi

    LOGMESSAGE << "Signal from Qml: Selected instrument number" << selectedindex;
    const QStringList filenames = mDownloader.getPathsOfDownloadedFiles();
    const int indexExternalMidi = filenames.size()+1;
    if (selectedindex > indexExternalMidi)
    {
        LOGWARNING << "Cannot load file with index" << selectedindex;
        return;
    }

    mMidiPlayerWasPlaying = mMidiPlayer.isPlaying();
    if (mMidiPlayerWasPlaying) mMidiPlayer.togglePlayPause();
    mSoundGenerator.suspend();
    mMidiMicrotonalConverter.activate(false);
    QThread::msleep(200); // Wait 200 msec for sound generator to shut down

    if (mInstrument.isLoading()) mInstrument.cancelLoading();

    if (selectedindex == 0)
    {
        LOGMESSAGE << "Constructing artificial sound";
        emit startLoadingInstrument("");
    }
    else if (selectedindex == indexExternalMidi)
    {
        mMidiMicrotonalConverter.activate(true);
    }
    else
    {
        const QString filename = filenames[selectedindex-1];
        LOGMESSAGE << "Loading file with index" << selectedindex;
        LOGMESSAGE << "Loading file" << filename;
        emit startLoadingInstrument(filename);
    }
    mSettings.setValue("controller/instrumentindex",selectedindex);
}


//-----------------------------------------------------------------------------
//                        Cancel the loading process
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Private slot: Cancel the loading process
///////////////////////////////////////////////////////////////////////////////

void Application::cancelLoading()
{
    if (mInstrument.isLoading())
    {
        LOGMESSAGE << "*** CANCEL LOADING INSTRUMENT ***";
        mInstrument.cancelLoading();
    }
}


//-----------------------------------------------------------------------------
//                              Loading finished
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Private slot: Loading finished
///////////////////////////////////////////////////////////////////////////////

void Application::loadingFinished (bool success)
{
    LOGMESSAGE << "Receiving signal that loading was finished";
    (void)success;
    mSoundGenerator.resume();
    if (mMidiPlayerWasPlaying) mMidiPlayer.togglePlayPause();
}


//-----------------------------------------------------------------------------
//                        New instrument downloaded
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Private slot: New instrument downloaded
///////////////////////////////////////////////////////////////////////////////

void Application::newInstrumentDownloaded (QString localpath)
{
    LOGMESSAGE << "Receive signal: New instrument downloaded:";
    LOGMESSAGE << localpath;
    int number = updateShownInstrumentNames().size();
    // If this is the first instrument download it automatically (+2)
    if (number==3)
    {
        LOGMESSAGE << "First instrument is now loaded automatically";
        setInstrumentSelector(1);
    }
}


//-----------------------------------------------------------------------------
//                        All instruments downloaded
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Private slot: All instrumenst downloaded
///////////////////////////////////////////////////////////////////////////////
void Application::allInstrumentsDownloaded()
{
    LOGMESSAGE << "Receive signal: Downloader shutdown, now update instruments";
    updateShownInstrumentNames();
}


//================================= AUDIO =====================================

//-----------------------------------------------------------------------------
//                      Shorten very long device names
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief  Shorten very long device names
/// \param device : String of the device name
/// \param truncation : Maximal number of characters
/// \return Shortened device name
///////////////////////////////////////////////////////////////////////////////

QString Application::shorten (const QString &device, int truncation) const
{
    QString dev = device;

    // Especially for Microsoft Windows:
    // If a short name is given in parentheses isolate that first
    int opening = dev.indexOf("(");
    int closing = dev.indexOf(")");
    if (opening>0 and closing>opening)
        dev = device.mid(opening+1,closing-opening-1);

    // In any case truncate
    if (dev.size() > truncation)
    {
        dev.remove(0,dev.size()-truncation);
        int pos1 = dev.indexOf("_");
        int pos2 = dev.indexOf("-");
        if (pos1>=0 and pos1<truncation/4) dev.remove(0,pos1+1);
        else if (pos2>=0 and pos2<truncation/4) dev.remove(0,pos2+1);
    }
    return dev;
}


//-----------------------------------------------------------------------------
//                Private slot: Select audio device parameters
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Private slot: Select audio parameters
/// \param devIndex
/// \param srIndex
/// \param buffersize
/// \param packetsize
///////////////////////////////////////////////////////////////////////////////

void Application::selectAudioParameters (int devIndex, int srIndex,
                                         int buffersize, int packetsize)
{
    AudioParameters parameters = mAudioOutput.getActualDeviceParameters();

    devIndex--; // Compensate leading entry "[Choose]"
    LOGSTATUS << "Select audio device: Selection index =" << devIndex;
    QStringList deviceNames = mAudioOutput.getListOfDevices();
    if (devIndex < deviceNames.size() and devIndex>=0)
    {
        parameters.deviceName = deviceNames[devIndex];
        LOGMESSAGE << "Selected device: " << parameters.deviceName;
    }

    LOGSTATUS << "Select sample rate: Selection index =" << srIndex;
    QList<int> listOfSampleRates = parameters.supportedSampleRates;
    if (srIndex >= 0 and srIndex < listOfSampleRates.size())
    {
        parameters.sampleRate = listOfSampleRates[srIndex];
        LOGMESSAGE << "Selected sample rate: Rate =" << parameters.sampleRate;
    }

    LOGMESSAGE << "Select buffer size:" << buffersize;
    parameters.bufferSize = buffersize;
    mSettings.setValue("audiooutput/buffersize",buffersize);

    LOGMESSAGE << "Select packet size:" << packetsize;
    mSoundGenerator.setMaxPacketSize(packetsize);
    mSettings.setValue("audiooutput/packetsize",packetsize);

    mAudioOutput.setWantedParameters(parameters);
    emit connectAudioDevice(true);
}


//-----------------------------------------------------------------------------
//              Private slot: New audio device added (test)
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief  Private slot: New audio device added (test)
/// \param list : List of strings of the newly connected audio devices
///////////////////////////////////////////////////////////////////////////////

void Application::onAudioDevicesAdded (QStringList list)
{
    LOGMESSAGE << "New audio device added" << list;
}


//-----------------------------------------------------------------------------
//           Private slot: An audio device has been removed (test)
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Private slot: An audio device has been removed (test)
/// \param list : List of strings of the removed audio devices
///////////////////////////////////////////////////////////////////////////////

void Application::onAudioDevicesRemoved(QStringList list)
{
    LOGMESSAGE << "Audio removed" << list;

}


//-----------------------------------------------------------------------------
//      Private slot: The list of availabel devices has changed (test)
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief  Private slot: The list of availabel devices has changed (test)
/// \param list : List of strings of the available audio devices
///////////////////////////////////////////////////////////////////////////////

void Application::onChangeOfAvailableAudioDevices(QStringList list)
{
    LOGMESSAGE << "List of audio devices changed:" << list;
    for (auto &e : list) e = shorten(e);
    list.prepend(tr("[Choose output device]"));
    QStringList devices = mAudioOutput.getListOfDevices();
    AudioParameters parameters = mAudioOutput.getActualDeviceParameters();
    int deviceIndex = devices.indexOf(parameters.deviceName);
    emit setAudioDeviceList (list,deviceIndex+1);

    QStringList model;
    for (auto &e : parameters.supportedSampleRates) model << QString::number(e);
    int index = parameters.supportedSampleRates.indexOf(parameters.sampleRate);
    emit setAudioSampleRates(model,index);
}


//-----------------------------------------------------------------------------
//             Slot: on current audio device parameters changed
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Private slot: on current audio device parameters changed
///////////////////////////////////////////////////////////////////////////////

void Application::onCurrentDeviceParametersChanged()
{
    LOGMESSAGE << "List of audio parameters changed.";
    AudioParameters parameters = mAudioOutput.getActualDeviceParameters();
    LOGMESSAGE << "DEVICE =" << parameters.deviceName << " SR =" << parameters.sampleRate;
    QStringList model;
    for (auto &e : parameters.supportedSampleRates) model << QString::number(e);
    int index = parameters.supportedSampleRates.indexOf(parameters.sampleRate);
    emit setAudioSampleRates(model,index);
}


//-----------------------------------------------------------------------------
//              Slot: on audio connection established
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Private slot: on audio connection established
/// \param success : True if connection was established successfully
///////////////////////////////////////////////////////////////////////////////

void Application::onConnectionSuccessfullyEstablished (bool success)
{
    LOGMESSAGE << "Audio connected = " << success;
    AudioParameters parameters = mAudioOutput.getActualDeviceParameters();
    QString dev = parameters.deviceName;
    if (dev.size()>0 and success) dev = shorten(dev);
    else dev = tr("No device connected");
    emit setAudioDeviceName(dev);
}


//-----------------------------------------------------------------------------
//                            Load a Midi file
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Load a Midi file
/// \param filename : Name of the Midi file
///////////////////////////////////////////////////////////////////////////////

bool Application::loadMidiFile (QString filename)
{
    LOGMESSAGE << "Trying to load Midi file" << filename;
    QFile midifile (filename);
    if (midifile.exists())
    {
        mMidiPlayer.loadFile(filename,true);
        return true;
    }
    else
    {
        LOGWARNING << "MIDI File" << filename << "does not exist";
        return false;
    }
}


//-----------------------------------------------------------------------------
//                    Debug: Show error message on mobile
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Show error message on mobile phone while it is connected to a
/// keyboard.
/// \param msg : Message
///////////////////////////////////////////////////////////////////////////////

void Application::mobileMessage(QString msg)
{
    emit sendMobileMessage(msg);
    qDebug() << "MOBILE ERROR MESSAGE" << msg;
}


//-----------------------------------------------------------------------------
//                    Slot: Closed Midi feedback loop discovered
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief  Slot: Closed Midi feedback loop discovered, inhibit converter
///////////////////////////////////////////////////////////////////////////////

void Application::onClosedMidiLoopDetected()
{
    mMidiMicrotonalConverter.activate(false);
    LOGWARNING << "Received signal about a closed Midi feedback loop";
    // TODO message
}


//-----------------------------------------------------------------------------
//                    Slot: Closed Midi feedback loop discovered
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief  I/O configuration changed, cancel converter inhibition
///////////////////////////////////////////////////////////////////////////////

void Application::onInputOutputStatusChanged()
{
    mMidiMicrotonalConverter.resume();
}


//-----------------------------------------------------------------------------
//                    Slot: Reset Audio Parameters
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief  Reset audio parameters
///////////////////////////////////////////////////////////////////////////////

void Application::resetAudioParameters()
{
    int buffersize = INT_BUFFERSIZE;
    int packetsize = INT_PACKETSIZE;

    auto parameters = mAudioOutput.getActualDeviceParameters();

    LOGMESSAGE << "Reset buffer size:" << buffersize;
    parameters.bufferSize = buffersize;
    mSettings.setValue("audiooutput/buffersize",buffersize);

    LOGMESSAGE << "Reset packet size:" << packetsize;
    mSoundGenerator.setMaxPacketSize(packetsize);
    mSettings.setValue("audiooutput/packetsize",packetsize);

    mAudioOutput.setWantedParameters(parameters);
    emit connectAudioDevice(true);
    emit setAudioBufferSizes (buffersize,packetsize);
}


//-----------------------------------------------------------------------------
//                    Debug: Show error message on mobile
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////

int mobileErrorCounter = 0;

void mobileError(QString msg)
{
    globalApplicationPointer->mobileMessage(QString::number(mobileErrorCounter++) + ": " + msg);
}



// TODO Importer workaround:
#include "instrument/wave.h"
void Wave::automaticCyclicMorphing(){}
