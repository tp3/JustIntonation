/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//                          Microtonal Midi Converter
//=============================================================================

#include "midimicrotonal.h"

#include <QTimer>

//-----------------------------------------------------------------------------
//                              Constructor
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Constructor, resetting member variables
///////////////////////////////////////////////////////////////////////////////

MidiMicrotonal::MidiMicrotonal()
    : mMidiMicrotonalHelper(this)
    , mRunning(false)
    , mActive(false)
    , mSuspended(false)
    , mLastChannelModeMessage(0)
    , mTimer()
{
    setModuleName ("MidiMicrotone");
    mMidiMicrotonalHelper.setVerbosity(getVerbosity());
    reInitializeConvertedMidiStream();
    connect(&mTimer,&QTimer::timeout,
            &mMidiMicrotonalHelper,&MidiMicrotonalHelper::sendInitialMidiCommands);
    // Update channels (intruments) after 5 seconds idle time
    mTimer.setInterval(5000);
}

//-----------------------------------------------------------------------------
//                            Closed loop marker
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Closed-loop marking event
/// \details The application sends this MIDI command to the output Midi
/// device from time to time. If this marker is received by the Midi input,
/// this indicates that the externally connected device simply forwards the
/// output back to the input, which would lead to a dangerous feedback loop
/// of Midi commend. This marker serves as a tag to discover such feedback
/// loops and to disconnect the output device in such a case.
///////////////////////////////////////////////////////////////////////////////
const QMidiMessage MidiMicrotonal::cLoopMarker(0xf2,0x1a,0x37,0.0);


//-----------------------------------------------------------------------------
//                                Init
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Initialization, no functionality
/// \return True
///////////////////////////////////////////////////////////////////////////////

bool MidiMicrotonal::init()
{ return true; }


//-----------------------------------------------------------------------------
//                                Exit
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Shutdown, no functionality
/// \return true
///////////////////////////////////////////////////////////////////////////////

bool MidiMicrotonal::exit()
{ return true; }


//-----------------------------------------------------------------------------
//                                Start
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Start the microtonal converter
/// \return true
///////////////////////////////////////////////////////////////////////////////

bool MidiMicrotonal::start()
{
    LOGMESSAGE << "Microtonal converter started";
    mRunning = true;
    mTimer.start();
    return true;
}


//-----------------------------------------------------------------------------
//                                Stop
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Stop the microtonal converter
/// \return true
///////////////////////////////////////////////////////////////////////////////

bool MidiMicrotonal::stop()
{
    if (mRunning)
    {
        mMidiMicrotonalHelper.localControl(true);
        allNotesOff();
        mRunning = false;
        LOGMESSAGE << "Microtonal converter stopped";
    }
    else LOGWARNING << "Microtonal converter is already off";
    return true;
}


//-----------------------------------------------------------------------------
//                                Suspend
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Suspend the microtonal converter
/// \details If a closed Midi loop is detected the converter is suspended.
/// In the suspend mode the input slot receiveMidiEvent() ignores any input.
///////////////////////////////////////////////////////////////////////////////

void MidiMicrotonal::suspend()
{
    mSuspended = true;
}


//-----------------------------------------------------------------------------
//                                 Resume
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Resume from suspend mode
///////////////////////////////////////////////////////////////////////////////

void MidiMicrotonal::resume()
{
    if (mSuspended)
    {
        mSuspended = false;
        mTimer.start();
    }
}




//-----------------------------------------------------------------------------
//                      Input slot: Receive Midi event
//-----------------------------------------------------------------------------
//////////////////////////////////////////////////////////////////////////////
/// \brief Activate or deactivate the module
/// \param active : true if on, false otherwise
//////////////////////////////////////////////////////////////////////////////
void MidiMicrotonal::activate (bool active)
{
    if (active == mActive) return;
    if (active)
    {
        mActive = true;
        resume();
        mMidiMicrotonalHelper.localControl(false);
    }
    else
    {
        mActive = false;
        mMidiMicrotonalHelper.localControl(true);
    }
}


//-----------------------------------------------------------------------------
//                      Input slot: Receive Midi event
//-----------------------------------------------------------------------------

void MidiMicrotonal::receiveMidiEvent (const QMidiMessage event)
{
    // If converter is not working return
    if (not isWorking())
    {
        LOGSTATUS << "Microtonal converter not working." << event;
        return;
    }

    // If a closed loop is detected suspend the converter and emit a signal
    if (event.message() == cLoopMarker.message())
    {
        LOGWARNING << "Detected closed Midi loop";
        suspend();
        //emit onClosedMidiLoopDetected();
        return;
    }

    LOGSTATUS << "Receive Midi event" << event;
    int channel = event.byte0() & 0x0f;
    quint8 code = event.byte0() & 0xf0;

    // if channel mode message
    QMutexLocker locker(&mMutex);

    if (code == 0xB0)
    {
        // Ignore two subsequent messages of the same type
        if (mLastChannelModeMessage == event.byte1()) return;
        mLastChannelModeMessage = event.byte1();
        switch (event.byte1())
        {
        //case 120: // All sound off immediately

        case 121: // Reset all controllers
        case 123: // All notes off including release
            mMidiMicrotonalHelper.allNotesOff();
            break;
        case 0:
        //case 7:
        case 0x40:
        case 0x41:
        case 0x42:
        case 0x43:
        case 0x44:
            mMidiMicrotonalHelper.controlChange(channel,event.byte1(),event.byte2(),event.timestamp());
            break;
        default:
            break;
        }
    }
    else // if no channel mode message
    {
        mLastChannelModeMessage = 0;
        // Lock access since different threads could call this slot
        // at the same time.
        switch (code)
        {
        case 0x80:
            mMidiMicrotonalHelper.turnNoteOff(channel,event.byte1(),event.timestamp());
            break;
        case 0x90:
            mMidiMicrotonalHelper.turnNoteOn(channel,event.byte1(),event.byte2(),event.timestamp());
            break;
        case 0xA0:
            //mConverter.afterTouch(channel,event.arg1,event.arg2,event.delta);
            break;
        case 0xB0: // this was done above
             break;
        case 0xC0:
            //mConverter.setInstrument(channel,event.arg1);
            break;
        case 0xE0:
            // Ignore pitch bends
            break;
        default:
            LOGMESSAGE << "Unrecognized Midi code" << event.byte0();
            break;
        }
    }
    // After each Midi event the timer is restarted. On timeout the channels
    // of the target device are set to the selected instrument
    mTimer.start();
}


//-----------------------------------------------------------------------------
//                      Input slot: Receive tuning corrections
//-----------------------------------------------------------------------------

void MidiMicrotonal::receiveTuningCorrections(QMap<int, double> corrections)
{
    if (not mRunning)
    {
        LOGSTATUS << "Microtonal converter is switched off";
        return;
    }
    LOGSTATUS << "tune" << corrections;
    QMapIterator<int, double> entry(corrections);

    // Lock access since different threads could call this slot
    // at the same time.
    QMutexLocker locker(&mMutex);
    while (entry.hasNext())
    {
        entry.next();
        mMidiMicrotonalHelper.tune(entry.key(),entry.value());
    }
}


//-----------------------------------------------------------------------------
//                      Input slot: Turn all notes off
//-----------------------------------------------------------------------------

void MidiMicrotonal::allNotesOff()
{
    QMutexLocker locker(&mMutex);
    if (not mRunning)
    {
        LOGSTATUS << "Microtonal converter is switched off";
        return;
    }
    LOGSTATUS << "Turn all notes off";
    // Lock access
    mMidiMicrotonalHelper.allNotesOff();
}

//-----------------------------------------------------------------------------
//                   Input slot: set the instrument channel
//-----------------------------------------------------------------------------

void MidiMicrotonal::setMidiOutputChannel(int channel)
{
    QMutexLocker locker(&mMutex);
    mMidiMicrotonalHelper.setInstrument(channel);
    mMidiMicrotonalHelper.sendInitialMidiCommands();
}


//-----------------------------------------------------------------------------
//                Input slot: Re-initialize Midi output strem
//-----------------------------------------------------------------------------

void MidiMicrotonal::reInitializeConvertedMidiStream()
{
    // Start sending intial string of Midi commands after a waiting time
    if (not isWorking())
    {
        LOGMESSAGE << "Re-initialize after I/O device change";
        resume();
    }
    QTimer::singleShot(200,&mMidiMicrotonalHelper,
                       &MidiMicrotonalHelper::sendInitialMidiCommands);
}
