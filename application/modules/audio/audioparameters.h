/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//        Structure holding the parameters and status of an audio device
//=============================================================================

#ifndef AUDIOPARAMETERS_H
#define AUDIOPARAMETERS_H

#include <QString>
#include <QList>

#include "system/shared.h"

///////////////////////////////////////////////////////////////////////////////
/// \ingroup audio
/// \brief Structure holding the parameters and status of an audio device
///////////////////////////////////////////////////////////////////////////////

struct AudioParameters
{
    QString deviceName;                 ///< Name of the audio device
    int channelCount;                   ///< Number of channels (mono=1, stereo=2)
    int sampleSize;                     ///< Sample size (16 or 24)
    int bufferSize;                     ///< Buffer size of the device if applicable
    int sampleRate;                     ///< Actual sample rate
    QList<int> supportedSampleRates;    ///< List of supported sample rates

    bool active;                        ///< True of device is active
    bool muted;                         ///< True if device is muted

    // Default void constructor and comparison function:
    AudioParameters();
    bool operator==(const AudioParameters& p) const;
};


#endif // AUDIOPARAMETERS_H
