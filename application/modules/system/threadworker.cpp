/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//                             Thread worker class
//=============================================================================

#include "threadworker.h"

#include <QDebug>
#include <QTimer>

#include "threadbase.h"

//-----------------------------------------------------------------------------
//                               Constructor
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Constructor
/// \details In the constructor the parameters are reset to default values,
/// namely, an empty thread name, normal execution priority,
/// and a timer interval of one second.
/// \param threadbase : Pointer back to the ThreadBase instance
///////////////////////////////////////////////////////////////////////////////

ThreadWorker::ThreadWorker (ThreadBase *threadbase)
    : QThread()
    , pThreadBase(threadbase)
    , mThreadName("")
    , mPriority(QThread::NormalPriority)
    , mInterval(1000)
    , mFirstInterval(0)
{
}


//-----------------------------------------------------------------------------
//                              Set priority
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Specify the priority of the thread
/// \details Can be used at any time, no matter whether the thread is running.
/// \param priority : Qt priority level
/// \see http://doc.qt.io/qt-5.7/qthread.html#Priority-enum
///////////////////////////////////////////////////////////////////////////////

void ThreadWorker::setPriority (const QThread::Priority priority)
{
    mPriority = priority;
    if (isRunning()) QThread::setPriority (mPriority);
}


//-----------------------------------------------------------------------------
//                           Set timer interval
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Set timer interval in milliseconds
/// \details Set the timer interval at which
/// the PeriodicallyCalledWorkerFunctiion()
/// will be called. Has to be called before the thread starts.
/// \param msec : Periodic waiting time in milliseconds
/// \param firstMsec : First waiting time in milliseconds
///////////////////////////////////////////////////////////////////////////////

void ThreadWorker::setTimerInterval(const int msec, const int firstMsec)
{
    mInterval = msec;
    mFirstInterval = firstMsec;
    if (isRunning())
        LOGWARNING << "setTimerInterval(...) called while thread is running";
}



//-----------------------------------------------------------------------------
//                             Set thread name
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Set thread name
/// \param name : Name to be assigned to the thread
/// \note : Threads can only be named under Linux
///////////////////////////////////////////////////////////////////////////////

void ThreadWorker::setThreadName(const QString name)
{
    mThreadName = name;
    if (mThreadName.size()>0 and isRunning())
        setCurrentThreadName(mThreadName);
}


//-----------------------------------------------------------------------------
//                           Start thread worker
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Start thread and move base class to the new thread
/// \details After starting the thread the instance of the base class
/// of type ThreadBase is moved to the thread of ThreadWorker. This ensures
/// that all slots of the base class and its derived classes are executed
/// in the new thread.
/// \return True on success
///////////////////////////////////////////////////////////////////////////////

bool ThreadWorker::start()
{
    setModuleName(pThreadBase->getModuleName());
    LOGMESSAGE << "Request to start thread, assigned name =" << mThreadName;
    QThread::start(mPriority);

    // If it does not start immediately wait 1msec
    if (not isRunning()) msleep(1);

    // If it does not start after 1 msec return error
    if (not isRunning())
    {
        LOGERROR << "Could not start the thread with the assigned name" << mThreadName;
        return false;
    }
    // All slots of the base class shall be executed in the new thread
    // This is essential since otherwise the components would
    // run in the original thread
    pThreadBase->moveToThread(this);
    return true;
}


//-----------------------------------------------------------------------------
//                       Request thread for termination
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Send a request to the thread for termination
/// \details This function requests the thread to terminate. The user has to
/// make sure that the implementation stops as soon as isInterruptionRequested()
/// becomes true. The calling thread waits up to two seconds for the thread
/// to terminate. If the thread terminates before the function returns true,
/// otherwise it returns false.
/// \return True if thread stopped regularly, False if thread is still
/// running after a timeout of two seconds.
///////////////////////////////////////////////////////////////////////////////

bool ThreadWorker::stop()
{
    LOGMESSAGE << "Request to terminate thread" << mThreadName;
    QThread::quit();    // Request thread to terminate
    return wait(2000);  // Wait for termination up to 2 seconds
}


//-----------------------------------------------------------------------------
//           Private thread worker function, starting execution loop
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Private thread worker function, starting execution loop
/// \details After creation the new thread executes this function. First the timer is
/// created. If the thread is in the suspended mode it simply waits.
/// Otherwise the timer and the execution loop are started.
///////////////////////////////////////////////////////////////////////////////

void ThreadWorker::run()
{
    if (mThreadName.size()>0) setCurrentThreadName(mThreadName);
    LOGSTATUS << "Thread is now running:" << getThreadName();
    QTimer timer, first;
    connect(&timer, &QTimer::timeout, pThreadBase, &ThreadBase::timeout);
    connect(&first, &QTimer::timeout, pThreadBase, &ThreadBase::timeout);
    first.setSingleShot(true);
    connect(this, SIGNAL(startTimer()),&timer,SLOT(start()));
    connect(this, SIGNAL(stopTimer()),&timer,SLOT(stop()));
    pThreadBase->initiallyCalledWorker();
    do
    {
        if (pThreadBase->mSuspended)
        {
            while (pThreadBase->mSuspended) msleep(100);
            LOGSTATUS << "Resume execution loop of thread" << getThreadName();
        }
        else { LOGSTATUS << "Entering execution loop of thread" << getThreadName(); }

        timer.start(mInterval);
        if (mFirstInterval>0) first.start(mFirstInterval);
        exec();
        LOGSTATUS << "Leaving execution loop of thread" << getThreadName();
        timer.stop();
        if (pThreadBase->mSuspended) LOGMESSAGE << "Thread suspended";
    }
    while (pThreadBase->mSuspended);
    pThreadBase->finallyCalledWorker();
    LOGSTATUS << "Shutting down thread" << getThreadName();
}



//-----------------------------------------------------------------------------
//                        Specify the current thread name
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Helper function for debugging: set the current thread name
/// \param threadname : QString holding the name of the thread
/// \note This function works only on Linux systems, it is inactive on other
/// platforms
///////////////////////////////////////////////////////////////////////////////

void ThreadWorker::setCurrentThreadName (QString threadname)
{
#if defined(Q_OS_LINUX) && !defined(Q_OS_ANDROID)
    pthread_setname_np (pthread_self(), threadname.toStdString().c_str());
    LOGMESSAGE << threadname.toStdString().c_str() << "thread started:"
               << getThreadName().toStdString().c_str();
#else
    (void)threadname;
    LOGMESSAGE << "Thread names are not avialable on this platform";
#endif
}

//-----------------------------------------------------------------------------
//                             Get thread name or id
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Helper function for debugging: Get the current thread id and time
/// \return Name of the thread in a readable format.
/// This name is the actual name of the thread. It may differ from the
/// name that was assigned via setThreadName(),
/// \note This feature works only on Linux systems, it is inactive on other
/// platforms
///////////////////////////////////////////////////////////////////////////////

QString ThreadWorker::getThreadName() const
{
    QString s;
#if defined(Q_OS_LINUX) && !defined(Q_OS_ANDROID)
    unsigned int id = (unsigned int)pthread_self();
    char buffer[100];
    pthread_getname_np(pthread_self(),buffer,100);
    s += "id = " + QString::number(id) + " (" + QString(buffer) + ")";
#else
    s = QString("(Thread name not available on this platform)");
#endif
    return s;
}


