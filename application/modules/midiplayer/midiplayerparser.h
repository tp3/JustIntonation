/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//                              Midi file parser
//=============================================================================

#ifndef MIDIPLAYERPARSER_H
#define MIDIPLAYERPARSER_H

#include <QByteArray>

#include "midiplayereventlist.h"

class MidiPlayer;

///////////////////////////////////////////////////////////////////////////////
/// \brief Midi file parser
/// \ingroup MidiPlayer
/// \details This function parses a Midi file sequentially. The specifications
/// can be found e.g. in https://www.midi.org/specifications.
/// The external user only has to call the constructor, passing a pointer to
/// the event list in which the Midi events are supposed to be stored.
/// Then call 'parse' with the file content of the Midi file stored in a
/// QByteArray. The function then parses the Midi file and stores the events
/// in the event list without distinguishing the tracks.
///////////////////////////////////////////////////////////////////////////////

class MidiPlayerParser : public QObject
{
    Q_OBJECT
public:
    MidiPlayerParser (MidiPlayer *player,
                      MidiPlayerEventList *eventlist, int verbosity=0);
    bool parse (QByteArray &data);

private:
    int findNextChunk (QByteArray &data, QString tag);
    quint32 parseInt32 (QByteArray &data);
    quint16 parseInt16 (QByteArray &data);
    quint32 parseVariableLengthNumber(QByteArray &data);
    bool parseTrack (QByteArray &data, int length, quint8 track);
    bool parseEvent (QByteArray &data, quint8 track, quint32 delta, double cumulativeDelta);
    bool parseMetaEvent (QByteArray &data,  quint8 track, double cumulativeDelta);
    void reportError (QString msg) const;

    MidiPlayer *pMidiPlayer;                ///< Pointer back to the MidiPlayer
    MidiPlayerEventList *pEventList;        ///< Pointer to event list held by MidiPlayer
    int mCursor;                            ///< Cursor from where the events are being played
    bool SMPTE;                             ///< Flag indicating the time format
    quint8 mLastMidiCommand;                ///< Variable storing the last Midi command
    int mVerbosity;                         ///< Level of verbosity of qDebug() messages
};

#endif // MIDIPLAYERPARSER_H
