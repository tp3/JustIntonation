/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//                              Midi file parser
//=============================================================================

#include "midiplayerparser.h"

#include <QDebug>

#include "midiplayer.h"
#include "midiplayereventlist.h"

//-----------------------------------------------------------------------------
//                              Constructor
//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
/// \brief Constructor
/// \param player : Pointer back to the MidiPlayer
/// \param eventlist : Pointer to the list of events
/// \param verbosity : Level of verbosity for qDebug messages
///////////////////////////////////////////////////////////////////////////////

MidiPlayerParser::MidiPlayerParser (MidiPlayer *player,
                                    MidiPlayerEventList *eventlist,
                                    int verbosity)
    : pMidiPlayer(player)
    , pEventList(eventlist)
    , mCursor(0)
    , SMPTE(false)
    , mLastMidiCommand(0)
    , mVerbosity(verbosity)

{}


//-----------------------------------------------------------------------------
//                            Parse main function
//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
/// \brief Parse a Midi file and store it in the EventList.
/// \details This is the main public function of the class which is called
/// from the MidiPlayer. The file has already been read and is now assumed to be
/// stored in a QByteArray (Midi files are usually very short so that this
/// is no problem). The function then parses the content of the file, looking
/// for headers and chunks. The keystrokes of all tracks are merged and mapped
/// to a single event list (a pointer to which was passed in the constructor).
/// \param data : Reference to QByteArray holding the file content.
/// \return True if parsing process was successful.
///////////////////////////////////////////////////////////////////////////////

bool MidiPlayerParser::parse(QByteArray &data)
{
    if (not pEventList) return false;
    if (mVerbosity >= 3) qDebug() << "parsing data file with" << data.length() << "bytes";
    pEventList->clear();

    // First look for the header chunk
    if (findNextChunk(data,"MThd") != 6)
        { reportError("Could not find MThd header"); return false; }
    if (mCursor+6 >= data.size())
        { reportError("Could not read MThd header"); return false; }
    quint16 format = parseInt16(data);
    quint16 tracks = parseInt16(data);
    quint16 division = parseInt16(data);

    // Filter supported formats
    if (format > 2)     { reportError("Unknown format in MThd header"); return false; }
    if (format == 2)    { reportError("Sequential track format not supported"); return false; }
    if (tracks == 0)    { reportError("No tracks in MIDI file"); return false; }
    if (tracks >= 256)  { reportError("Too many tracks"); return false; }
    SMPTE = ((division & 0x8000U) != 0);

    // Report identified track to qDebug
    if (mVerbosity >= 3)
        qDebug() << "Detected" << (format==0 ? "single" : "multi") << "channel MIDI data with"
             << tracks << "tracks in" << (SMPTE ? "absolute SMPTE-based" :  "tempo-based") << "division";

    // If SMPTE (absolute times) calculate ticks per quarter now
    if (SMPTE)
    {
        qint8 negFrames = division>>8;
        int frames = abs(negFrames);
        int ticksPerFrame = division & 0xFFU;
        double msecPerDeltaTick = 1000.0 / frames / ticksPerFrame;
        if (pMidiPlayer) pMidiPlayer->receiveTempoData(true,msecPerDeltaTick);
        if (mVerbosity >= 3) qDebug() << "SMPTE: frames =" << frames << "/ Tick per frame"
                                      << ticksPerFrame << "/ msec per tick =" << msecPerDeltaTick;
    }
    else // if tempo-based
    {
        int ticksPerQuarterNote = division;
        if (mVerbosity >= 3) qDebug() << "Midi file is tempo-based with"
                      << ticksPerQuarterNote << "ticks per quarter note";
        if (pMidiPlayer) pMidiPlayer->receiveTempoData(false,ticksPerQuarterNote);
    }

    // Loop over all tracks
    for (int track=0; track<tracks; track++)
    {
        int length = findNextChunk(data,"MTrk");
        if (length == 0)  { reportError("Could not read MThd header"); return false; }
        if (not parseTrack(data,length,track))
                          { reportError("Could not read track " + QString::number(track)); }
    }
    //pEventList->writeListInReadableForm("/home/hinrichsen/eventlist.txt");
    return true;
}


//-----------------------------------------------------------------------------
//                        Find the next chunk tag
//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
/// \brief Find the next chunk tag
/// \details Midi files are orginized in terms of 'chunks'. Each chunk starts
/// with a 4-letter ASCII tag, e.g. "MThd" or "MTrk". This function looks
/// for such a tag starting at the current cursor position, moves the cursor
/// to the starting point of the chunk data and returns the length of the
/// chunk data block if successful.
/// \param data : Reference QByteArray holding the file content
/// \param tag : Four-letter ASCII tag
/// \return : length of the data chunk, 0 if not found
///////////////////////////////////////////////////////////////////////////////

int MidiPlayerParser::findNextChunk (QByteArray &data, QString tag)
{
    QByteArray pattern(tag.toStdString().c_str());
    int position = data.indexOf(pattern,mCursor);
    if (position<0 or position>=data.size()-8) return 0;
    mCursor = position + 4;
    int length = parseInt32(data);
    if (mVerbosity >= 3)
        qDebug() << "Chunk" << tag << "found at position"
                 << position << "with" << length << "bytes";
    return length;
}


//-----------------------------------------------------------------------------
//                          Parse a 32bit integer
//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
/// \brief Parse a 32-bit (4-byte) integer and advance the cursor
/// \param data : Reference QByteArray holding the file content
/// \return Value of the integer
///////////////////////////////////////////////////////////////////////////////

quint32 MidiPlayerParser::parseInt32 (QByteArray &data)
{
    mCursor += 4;
    if (mCursor > data.size())
        { reportError("Reading int32 beyond EOF"); return 0; }
    quint32 byte0 =(quint8)data[mCursor-1];
    quint32 byte1 =(quint8)data[mCursor-2];
    quint32 byte2 =(quint8)data[mCursor-3];
    quint32 byte3 =(quint8)data[mCursor-4];
    quint32 result = byte0 + (byte1<<8) + (byte2<<16) + (byte3<<24);
    return result;
}


//-----------------------------------------------------------------------------
//                          Parse a 16bit integer
//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
/// \brief Parse a 16bit (2-byte) integer and advance the cursor
/// \param data : Reference QByteArray holding the file content
/// \return Value of the integer
///////////////////////////////////////////////////////////////////////////////

quint16 MidiPlayerParser::parseInt16 (QByteArray &data)
{
    mCursor += 2;
    if (mCursor > data.size())
        { reportError("Reading int16 beyond EOF"); return 0; }
    quint16 lsb = (quint8)data[mCursor-1];
    quint16 msb = (quint8)data[mCursor-2];
    quint16 result = lsb + (msb<<8);
    return result;
}


//-----------------------------------------------------------------------------
//                     Parse a variable-length integer
//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
/// \brief Parse a variable-length integer
/// \details The Midi specifications involve the use of so-called variable
/// length integers. In these integers the MSB of each byte is used to
/// indicate whether another byte will follow. This allows Midi files to
/// save space since most of the time intervals are very short. The
/// actual range of integers is in practice limited to 32 bit.
/// \param data : Reference QByteArray holding the file content
/// \return : Value of the integer
///////////////////////////////////////////////////////////////////////////////

quint32 MidiPlayerParser::parseVariableLengthNumber (QByteArray &data)
{
    quint32 number = 0;
    do
    {
        mCursor++;
        if (mCursor > data.size()) return 0;
        number <<= 7;
        number += (data[mCursor-1] & 0x7FU);
    }
    while ((data[mCursor-1] & 0x80U) != 0);
    return number;
}


//-----------------------------------------------------------------------------
//                           Parse an entire track
//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
/// \brief Parse an entire track
/// \details A track consists of a variable-length integer holding the time
/// elapsed since the last event, followed by an event (see parseEvent(...)).
/// This function reads these pairs subsequently until the expected length
/// has been read.
/// \param data : Reference QByteArray holding the file content
/// \param length : Expected length of the track
/// \param track : The track number
/// \return : True if successfull
///////////////////////////////////////////////////////////////////////////////

bool MidiPlayerParser::parseTrack (QByteArray &data, int length, quint8 track)
{
    mLastMidiCommand = 0;
    int finalPosition = mCursor + length;
    double cumulativeDelta = 0;
    do
    {
        quint32 delta = parseVariableLengthNumber(data);
        // The following small offset guarantees the correct order in the list
        cumulativeDelta += delta + 0.0000001;
        if (not parseEvent(data, track, delta, cumulativeDelta)) return false;
        if (mCursor > finalPosition) return false;
    }
    while (mCursor < finalPosition);
    return true;
}


//-----------------------------------------------------------------------------
//                          Parse a single event
//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
/// \brief Parse a single event
/// In the Midi specifications three types of events are defined, namely,
/// ordinary Midi events, sysex events, and meta events. This function detects
/// the event type and takes action accordingly.
/// \param data : Reference QByteArray holding the file content
/// \param track : The track number
/// \param delta : Value of delta preceding the actual event.
/// \param cumulativeDelta : Sum of all preceding deltas
/// \return True if successful
////////////////////////////////////////////////////////////////////////////////

bool MidiPlayerParser::parseEvent (QByteArray &data, quint8 track,
                                   quint32 delta, double cumulativeDelta)
{
    // Read the command byte
    if (mCursor >= data.size()) return false;
    quint8 command = data[mCursor];
    mCursor++;

    // Midi commands start with bit7 set. Sometimes this convention is broken in
    // the following sense: If the parser expects a Midi command but finds
    // instead a byte with bit7=0, then the last Midi command is used.
    if ((command & 0x80U) == 0 and (mLastMidiCommand  & 0x80U) > 0)
    {
        command = mLastMidiCommand;
        mCursor--;
    }
    else mLastMidiCommand = command;

    // Identify the type of the event
    enum commandType {undefined,ignore,onebyte,twobytes,sysex,meta};
    commandType type = undefined;
    if (command <= 0xBFU) type = twobytes; // 80-BF
    else if (command <= 0xDFU) type = onebyte; // C0-DF
    else if (command <= 0xEFU) type = twobytes; // E0-EF
    else if (command == 0xF0U) type = sysex;
    else if (command == 0xF2U) type = twobytes;
    else if (command == 0xF3U) type = onebyte;
    else if (command == 0xFFU) type = meta;
    else if (mVerbosity >= 3) qDebug() << "MidiParser: Unrecognized command code =" << command
                                      << " ... assuming it to be 1-byte dummy code";

    // Take action accordingly
    switch(type)
    {
    case undefined:
        if (mVerbosity >= 2) qWarning() << "WARNING: MidiParser: Unrecognized command code =" << command;
        for (int i=-10; i<10; i++) qDebug() << (quint8)(data[mCursor+i]);
        return false;
    case ignore:
        return true;
    case onebyte:
    {
        if (mCursor >= data.size()) return false;
        MidiPlayerEvent event(track,delta,command,data[mCursor],0);
        if (pEventList) pEventList->insert(cumulativeDelta,event);
        mCursor++;
        return true;
    }
    case twobytes:
    {
        if (mCursor+1 >= data.size()) return false;
        MidiPlayerEvent event(track,delta,command,data[mCursor],data[mCursor+1]);
        if (pEventList) pEventList->insert(cumulativeDelta,event);
        mCursor+=2;
        return true;
    }
    case sysex:
        qDebug() << "SYSEX NOT YET IMPLEMENTED";
        return false;
    case meta:
        return parseMetaEvent(data,track,cumulativeDelta);
    }
    return false;
}


//-----------------------------------------------------------------------------
//               Parse a meta event and display info in qDebug
//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
/// \brief Parse a meta event and display info in qDebug
/// \param data : Reference QByteArray holding the file content
/// \param track : Midi track number
/// \param cumulativeDelta : Cumulative delta time parameter
/// \return True if successful
///////////////////////////////////////////////////////////////////////////////

bool MidiPlayerParser::parseMetaEvent (QByteArray &data,  quint8 track, double cumulativeDelta)
{
    if (mCursor >= data.size()) return false;
    quint8 command = data[mCursor];
    mCursor++;
    quint32 length = parseVariableLengthNumber(data);
    if (command == 0x2FU and length == 0) // EOT
    {
        if (mVerbosity >= 3) qDebug() << "MidiPlayerParser: End of track detected as meta event";
    }
    else if (command >= 0x01U and command <= 0x07U) // Text message
    {
        QByteArray sub = data.mid(mCursor,length);
        if (mVerbosity >= 3) qDebug() << "MidiPlayerParser: Track info:\n" << sub.data();
    }
    else if (command == 0x51 and length == 3) // Tempo setting
    {
        quint32 byte0 = (quint8)data[mCursor+2];
        quint32 byte1 = (quint8)data[mCursor+1];
        quint32 byte2 = (quint8)data[mCursor];
        quint32 result = byte0 + (byte1<<8) + (byte2<<16);

        int millisecondsPerQuarterNote = result / 1000;
        if (mVerbosity >= 3) qDebug() << "MidiPlayerParser: Tempo:"
                << millisecondsPerQuarterNote << "msec per quarter note";

        // Custom Non-Midi event transmitting tempo changes
        MidiPlayerEvent event(track,0,0xFF,
                              millisecondsPerQuarterNote >> 8,
                              millisecondsPerQuarterNote & 0xFF);
        if (pEventList) pEventList->insert(cumulativeDelta,event);
    }
    else
    {
        if (mVerbosity >= 4) qDebug() << "Skipping meta event";
    }
    mCursor += length;
    if (mCursor > data.size()) return false;
    return true;
}


//-----------------------------------------------------------------------------
//                Report an error back to MidiPlayer
//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
/// \brief  Report an error back to MidiPlayer
/// \param msg : Error message string
///////////////////////////////////////////////////////////////////////////////

void MidiPlayerParser::reportError(QString msg) const
{
    if (pMidiPlayer) pMidiPlayer->reportError(msg);
}


