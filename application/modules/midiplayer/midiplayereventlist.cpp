/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//                      Midi Player Events and Event List
//=============================================================================

#include "midiplayereventlist.h"

#include <QFile>
#include <QTextStream>

//=============================================================================
//                      Midi Player Event constructor
//=============================================================================

///////////////////////////////////////////////////////////////////////////////
/// \brief Constructor for a MidiPlayerEvent, copying the arguments.
/// \param track : The midi track
/// \param deltaticks : Time ticks elapsed since the last event
/// \param command : Midi Command byte
/// \param byte1 : Midi first argument byte
/// \param byte2 : Midi second argument byte (zero if none)
////////////////////////////////////////////////////////////////////////////////

MidiPlayerEvent::MidiPlayerEvent(quint8 track, double deltaticks,
                                 quint8 command, quint8 byte1, quint8 byte2)
{
    this->track = track;
    this->deltaticks = deltaticks;
    this->command = command;
    this->byte1 = byte1;
    this->byte2 = byte2;
}


//*****************************************************************************

//------------------------------------------------------------------------------
//                               Constructor
//------------------------------------------------------------------------------

////////////////////////////////////////////////////////////////////////////////
/// \brief Constructor, resetting member variables.
////////////////////////////////////////////////////////////////////////////////

MidiPlayerEventList::MidiPlayerEventList()
    : mEventList()
    , mMaxTime(0)
    , pEventIterator(mEventList.begin())
    , mAccessMutex()
{}


//------------------------------------------------------------------------------
//                                  Clear
//------------------------------------------------------------------------------

////////////////////////////////////////////////////////////////////////////////
/// \brief Clear the event list (clear all Midi data).
////////////////////////////////////////////////////////////////////////////////

void MidiPlayerEventList::clear()
{
    mEventList.clear();
    pEventIterator = mEventList.begin();
    mMaxTime = 0;
}

//------------------------------------------------------------------------------
//           Insert a new event in the time-ordered list of events
//------------------------------------------------------------------------------

////////////////////////////////////////////////////////////////////////////////
/// \brief Insert a new event in the time-ordered list of events.
/// \param time : Integer time in ticks used for time-ordering.
/// \param event : New event to be inserted.
////////////////////////////////////////////////////////////////////////////////

void MidiPlayerEventList::insert(double time, const MidiPlayerEvent &event)
{
    QMutexLocker locker (&mAccessMutex);
    mEventList.insert(time,event);
    if (time > mMaxTime) mMaxTime = time;
}


//------------------------------------------------------------------------------
//                                  Rewind
//------------------------------------------------------------------------------

////////////////////////////////////////////////////////////////////////////////
/// \brief Rewind: Move the iterator back to the beginning of the Midi song.
////////////////////////////////////////////////////////////////////////////////

void MidiPlayerEventList::rewind()
{
    QMutexLocker locker (&mAccessMutex);
    pEventIterator = mEventList.begin();
}


//------------------------------------------------------------------------------
//                            Advance iterator
//------------------------------------------------------------------------------

////////////////////////////////////////////////////////////////////////////////
/// \brief Advance iterator in the list of events.
/// \details This function simply advances the iterator in the list by one step.
/// It is called repeatedly while the player is playing.
////////////////////////////////////////////////////////////////////////////////

void MidiPlayerEventList::advance()
{
    QMutexLocker locker (&mAccessMutex);
    pEventIterator++;
}


//------------------------------------------------------------------------------
//                      Are we at the end of the list?
//------------------------------------------------------------------------------

////////////////////////////////////////////////////////////////////////////////
/// \brief Predicate, telling us whether we are already at the end of the list.
/// \return True if at the end, False otherwise.
////////////////////////////////////////////////////////////////////////////////

bool MidiPlayerEventList::atEnd() const
{
    QMutexLocker locker (&mAccessMutex);
    return pEventIterator == mEventList.end();
}


//------------------------------------------------------------------------------
//                            Get actual event
//------------------------------------------------------------------------------

////////////////////////////////////////////////////////////////////////////////
/// \brief Return a copy of the actual event
/// (zero event if iterator is at the end of the list).
/// \return Actual event in the list targeted by the iterator.
////////////////////////////////////////////////////////////////////////////////

const MidiPlayerEvent MidiPlayerEventList::getEvent() const
{
    QMutexLocker locker (&mAccessMutex);
    if (pEventIterator == mEventList.end()) return MidiPlayerEvent();
    else return pEventIterator.value();
}


//------------------------------------------------------------------------------
//                               Get actual time
//------------------------------------------------------------------------------

////////////////////////////////////////////////////////////////////////////////
/// \brief Return time of actual event
/// (zero if iterator is at the end of the list).
/// \return Time (in ticks) of the actual event targeted by the iterator
////////////////////////////////////////////////////////////////////////////////

quint32 MidiPlayerEventList::getCumulativeTime() const
{
    QMutexLocker locker (&mAccessMutex);
    if (pEventIterator == mEventList.end()) return mMaxTime;
    else return pEventIterator.key();
}


//------------------------------------------------------------------------------
//                           Get actual list size
//------------------------------------------------------------------------------

////////////////////////////////////////////////////////////////////////////////
/// \brief Get actual list size.
/// \return Number of events in the list.
////////////////////////////////////////////////////////////////////////////////

int MidiPlayerEventList::getSize() const
{
    QMutexLocker locker (&mAccessMutex);
    return mEventList.size();
}


//-----------------------------------------------------------------------------
//                   Get the playing progress in percent
//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
/// \brief Get the playing progress in percent.
/// \return Temporal progress in percent.
///////////////////////////////////////////////////////////////////////////////

double MidiPlayerEventList::getProgressInPercent() const
{
    QMutexLocker locker (&mAccessMutex);
    if (pEventIterator == mEventList.begin()) return 0;
    else if (pEventIterator == mEventList.end()) return 100;
    else if (mMaxTime<=0) return 0;
    else return 100.0*pEventIterator.key()/mMaxTime;
}


//-----------------------------------------------------------------------------
//     Set the EventList iterator according to a given progress in percent
//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
/// \brief Set the EventList iterator according to a given progress in percent.
/// \param percent : Temporal progress in percent.
///////////////////////////////////////////////////////////////////////////////

void MidiPlayerEventList::setProgress (double percent)
{
    QMutexLocker locker (&mAccessMutex);
    if (mEventList.empty() or mMaxTime <= 0) return;
    pEventIterator = mEventList.begin();
    quint32 threshold = mMaxTime * percent / 100.0;
    while (pEventIterator != mEventList.end()
           and pEventIterator.key() < threshold) pEventIterator++;
}


//-----------------------------------------------------------------------------
//       Debugging: Write content of the list to file in a readable form
//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
/// \brief Write content of the list to file in a readable form (for debugging).
/// \param filename : Name of the file
///////////////////////////////////////////////////////////////////////////////

void MidiPlayerEventList::writeListInReadableForm(QString filename)
{
    QFile file(filename);
    if (not file.open(QIODevice::WriteOnly)) return;
    QTextStream out(&file);
    for (MidiPlayerEvent &e: mEventList)
    {
        out << "t=" << e.deltaticks << "\t" << "tr=" << e.track << "\tcmd=" << e.command
            << "\tb1="<< e.byte1  << "\tb2="<< e.byte2 << "\n";
    }
    if (file.isOpen()) file.close();
}
