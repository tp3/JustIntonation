/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

#ifndef TUNERKEYDATA_H
#define TUNERKEYDATA_H

#include <QVector>

///////////////////////////////////////////////////////////////////////////////
/// \brief Structure holding the tuner's data of a single key
/// \ingroup tuner
/// \details The tuner keeps track of each pressed key. This structure holds
/// the essential data associated with the pressed key.
///////////////////////////////////////////////////////////////////////////////

struct KeyData
{
    int key;                        ///< Number of the key
    bool pressed;                   ///< Flag indicating a pressed key
    bool newlyPressed;              ///< Flag indicating a newly pressed key
    bool emitted;                   ///< Flag indicating first emitted tuning
    double intensity;               ///< Intensity (volume) I(t)
    double memory;                  ///< Psychoacoustic memory M(t)
    double sustainDampingFactor;    ///< Sustain damping of the key
    double releaseDampingFactor;    ///< Release damping of the key
    qint64 timeStamp;               ///< Time when the key was pressed
    double pitch;                   ///< Actual pitch
    double previousPitch;           ///< Previously emitted pitch

    KeyData() { clear(); }          ///< Constructor, clearing all data

    /// \brief Function for clearing all fields in the TunerKeyData structure
    void clear()
    {
        key=0;
        pressed = newlyPressed = emitted = false;
        intensity = memory = pitch = previousPitch = 0;
        sustainDampingFactor = 1;
        releaseDampingFactor = 0;
        timeStamp = 0;
    }
};

using KeyDataVector = QVector<KeyData>;  ///< Data of all keys of the keyboard

#endif // TUNERKEYDATA_H
