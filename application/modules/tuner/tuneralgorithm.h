/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//                          Main Tuning Algorithm
//=============================================================================

#ifndef TUNERALGORITHM_H
#define TUNERALGORITHM_H

#include <QObject>
#include <QVector>
#include <QTimer>

#include "tunerkeydata.h"

#include "../../thirdparty/eigen/Eigen/Dense"
#include "../../thirdparty/eigen/Eigen/QR"

class Tuner;

///////////////////////////////////////////////////////////////////////////////
/// \brief Main Tuning Algorithm
/// \ingroup tuner
/// \details This class contains the main tuning algorithm.
///
/// The static tuning function is trivial: It simply copies the given pitches
/// in the argument to the corresponding keys.
///
/// The dynamical tuning function can operate in two different modes. If the
/// flag 'optimizedJI' is false, then the algorithm tunes dynamically
/// according to the given list of pitches and weigths in the arguments.
/// If the flag is true, then the list of pitches is ignored and the chord
/// is tuned according to the best possible choice of interval sizes from
/// the list defined at the end of this file (cJustIntonation).
///////////////////////////////////////////////////////////////////////////////

class TunerAlgorithm : public QObject
{
    Q_OBJECT
public:
    TunerAlgorithm ();

    void setIntervalSize   (int halftones, double cents);
    void setIntervalWeight (int halftones, double weight);

    void tuneStatically  (KeyDataVector &keys,
                        const QVector<double> pitches,
                        const int referenceKey,
                        const int wolfsIntervalShift);

    double tuneDynamically (KeyDataVector &keyData,
                            const QVector<double> intervals,
                            const QVector<double> weights,
                            bool optimizedJI);

private:
    Eigen::MatrixXi mSelectedVariant;           ///< Previously used pitch variant
    double mProgression;                        ///< Actual pitch progression

    const double octaveWeightFactor = 0.5;
    const double memoryWeight = 0.00005;
    const double epsilon = 1E-10;


    // The following array holds the list of pitch alternatives for
    // the twelve intervals in Just Intonation:
    const QVector<QVector<double>> cJustIntonation =
        {{0},                           ///< unison and octaves
         {11.7313,-7.821},              ///< semitone 16/15 and 135/128
         {3.9100, -17.5963},            ///< second 9/8 and 10/9
         {15.6413},                     ///< minor third 6/5
         {-13.6863},                    ///< major third 5/4
         {-1.9550},                     ///< perfect fourth 4/3
         {-9.77628},                    ///< tritone 45/32
         {1.9559},                      ///< perfect fifth 3/2
         {13.6863},                     ///< minor sixth 8/5
         {-15.6413},                    ///< major sixth 5/3
         {-3.9100, 17.5963, -31.1741},  ///< minor seventh 16/9, 9/5, and natural seventh 7/4
         {-11.7313}};                   ///< major seventh 15/8
};

#endif // TUNERALGORITHM_H
