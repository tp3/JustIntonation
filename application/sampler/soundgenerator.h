/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//            SoundGenerator - manage the tones that are being played
//=============================================================================

#ifndef SOUNDGENERATOR_H
#define SOUNDGENERATOR_H

#include <QQueue>
#include <QMutex>

#include "request.h"
#include "tone.h"
#include "sampler.h"

#include "modules/system/threadbase.h"

class Application;

///////////////////////////////////////////////////////////////////////////////
/// \brief Class managing the generation of sound
///////////////////////////////////////////////////////////////////////////////

class SoundGenerator : public ThreadBase
{
    Q_OBJECT
public:
    SoundGenerator();

    void init (Application *application, Voice *voice);
    bool init() override { return true; }
    void suspend() override final;

    void setMaxPacketSize (int n) { mSampler.setMaxPacketSize(n); }
    size_t getMaxPacketSize() { return mSampler.getMaxPacketSize(); }


    void registerRequest (Request &request);
    void registerAllNotesOff();

    Sampler& getSampler() { return mSampler; }
    QList<Tone>* getTones() { return &mTones; }
    void lockTones() { mToneMutex.lock(); }
    void unlockTones() { mToneMutex.unlock(); }

    Application* getApplication() { return pApplication; }

    quint32 getIncrement(int key);
    void setIncrement(int key, quint32 increment);
    void clearIncrements();

public slots:
    void receiveTuningCorrections(QMap<int, double> corrections);
    void setSustainPedal (bool pressed);
    void setSostenutoPedal (bool pressed);
    void setSoftPedal (bool pressed);

private slots:
    void timeout() override final;

signals:
    void forceImmediateProcessing();

private:
    void handleRequest (Request &request);
    void manageToneDynamics();
    bool queueFilled();


    using State = Tone::State;

    QQueue<Request> mQueue;
    QMutex mQueueMutex;

    QList<Tone> mTones;
    QMutex mToneMutex;

    QVector<quint32> mIncrements;
    QMutex mIncrementMutex;

    Sampler mSampler;

    Voice* pVoice;
    Application* pApplication;

    // TODO pedals
    bool mSustainPedeal;
    bool mSostenutoPedal;
    bool mSoftPedal;
};

#endif // SOUNDGENERATOR_H
