/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//                                   Tone
//=============================================================================

#ifndef TONE_H
#define TONE_H

#include <QtGlobal>

#include "instrument/wave.h"

///////////////////////////////////////////////////////////////////////////////
/// \brief Class describing a tone that is currently being played
/// \details This class describes the properties of a tone that is currently
/// played by the Sampler.
///
/// The microtonal tuning is controlled by two essential variables named
/// mTime and mIncrement. The standard value of mIncrement is 0x1000=4096
/// which corresponds to the standard frequency of the tone. For each frame
/// (i.e. for each PCM value) the sampler increases the mTime-Variable by
/// mIncrement. Microtonal tuning is achieved by changing mIncrement. For
/// example, going from 0x1000 to 0x1001 the frequency in increased by
/// the ratio 1/0x1000 = 1/4096.
///////////////////////////////////////////////////////////////////////////////

class Tone
{
public:
    enum Type
    {
        SUSTAIN_ENDING,         ///< Sound that ends when the key is released
        SUSTAIN_NONENDING,      ///< Sound that does not end when key is release (keys without damper)
        RELEASETONE,            ///< Sound that occurs in the moment when the key is released
        UNDEFINED               ///< non defined
    };

    enum State
    {
        IDLE,                   ///< Undefined
        PLAYING,                ///< Tone is playing in normal mode
        RELEASE_REQUESTED,      ///< The key has been released. Tone is requested to switch to the release mode.
        RELEASED,               ///< Release mode: tone is decaying
        OFF_PLAYING,            ///< Tone is playing a special off-sound after key has been released
        TERMINATED              ///< Tone has terminated and can be removed from the queue
    };


public:
    Tone();
    Tone (int key, int scaleindex, Wave &sample, double volume, Type type=UNDEFINED);

    friend class Sampler;       ///< Sampler class is allowed to access private elements
    friend class SoundGenerator;///< SoundGenerator class is allowed to access private elements
    friend class TuningManager; ///< TuningManager class is allowed to access private elements

    Wave &getSample() { return *pSample; }

private:
    State mState;               ///< Phase in which the tone is playing
    int mKey;                   ///< Key index (Midi) ranging from 0 to 127
    quint32 mTime;              ///< time in units of 1/4096/SampleRate sec

    double mVolume;             ///< overall volume ranging from 0 to 1

    qint16 left,right;          ///< store last PCM values, init to zero

    Wave *pSample;              ///< Pointer to the wave
    Type mType;                 ///< Type of the wave
    int mScaleIndex;            ///< Index to the used scale
};

#endif // TONE_H
