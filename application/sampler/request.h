/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//                  Class describing the request to play a note
//=============================================================================

#ifndef REQUEST_H
#define REQUEST_H


///////////////////////////////////////////////////////////////////////////////
/// \brief Structure containing the data for a play-note request
/// \details The SoundGenerator fills a queue (mQueue) of Requests to play a
/// or to stop a note or other controlling commands. The Sampler executes
/// these commands as soon as possible in an independent thread.
/// \note This structure has no implementation file.
//////////////////////////////////////////////////////////////////////////////

struct Request
{
    enum Command
    {
        NOTE_ON,            ///< Play a note with parameters listed below
        NOTE_OFF,           ///< Stop playing the note
        ALL_NOTES_OFF,      ///< Stop all notes
        OVERALL_PITCH_UP,   ///< Adiabatically drive the overall pitch up
        OVERALL_PITCH_DOWN, ///< Adiabatically drive the overall pitch down
        UNDEFINED
    };

    Command command;        ///< command
    int channel;            ///< MIDI channel (0..15)
    int key;                ///< Key index according to MIDI norm (0..127)
    double intensity;       ///< Volume
    int data;               ///< multipurpose data (do we really need that)
};

#endif // REQUEST_H
