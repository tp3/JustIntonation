/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//                                  Scale
//=============================================================================

#include "scale.h"

#include <QDebug>
#include <QThread>

#include "instrumentfilehandler.h"
#include "voice.h"

#define STATUS  if (Voice::mVerbosity >= 4) qDebug() << "Scale:"
#define MESSAGE if (Voice::mVerbosity >= 3) qDebug() << "Scale:"
#define WARNING if (Voice::mVerbosity >= 2) qWarning()<<"Scale: WARNING:"
#define ERROR   if (Voice::mVerbosity >= 1) qCritical() << "Scale: ERROR:"

//-----------------------------------------------------------------------------
//                              Access function
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Constructor, creates N=128 empty wave objects
///////////////////////////////////////////////////////////////////////////////

Scale::Scale() : mWaves(N) , mCancellationRequested(false)
{}


//-----------------------------------------------------------------------------
//                              Access function
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Get the wave corresponding to a given key
/// \param key : Index of the key in the range 0...127
/// \return Reference to the wave object
///////////////////////////////////////////////////////////////////////////////

Wave &Scale::getWave(int &key)
{ return mWaves[key & 0x7F]; }


//-----------------------------------------------------------------------------
//                        Compute artificial sounds
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Generate a scale of artificial sounds in equal temperament
/// \param voice : Pointer back to the voice, needed to report progress
/// \param samplerate : Sample rate used for generation, 44100 by default
///////////////////////////////////////////////////////////////////////////////

void Scale::generateArtificialSound (Voice* voice, int samplerate)
{
    mCancellationRequested = false;
    const double A4 = 440;
    double progress=0;
    for (int n=21; n<=108; ++n)
    {
        progress += 1.0/88;
        if (voice) voice->setProgress(QVariant(progress));
        if (QThread::currentThread()->isInterruptionRequested()) return;
        double frequ = A4*pow(2,(n-69.0)/12.0);
        mWaves[n].computeTriangularWave (frequ,samplerate,(n-20)/88.0);
        if (mCancellationRequested) break;
    }
}


//-----------------------------------------------------------------------------
//                 Insert new wave data (called from importer)
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Insert a newly recorded or generated waveform into the scale.
/// This function checks the parameters and calls the insert function of
/// the corresponding wave object.
/// \param key : Index of the key according to MIDI norm in the range 0..127
/// \param L : int32 vector containing the PCM wave of the left channel
/// \param R : int32 vector containing the PCM wave of the right channel
/// \param release : True if the sound is a release sound (the sound that
/// you hear when the key is released, e.g. how the damper settles)
/// \param amplification : Overall amplification factor
/// \return : True on success
///////////////////////////////////////////////////////////////////////////////

bool Scale::insert (const int key,
                    const QVector<qint32> &L,
                    const QVector<qint32> &R,
                    const bool release,
                    const double amplification)
{
    if (key<0 or key>127)
    {
        qDebug() << "Scale::insert: key" << key << "out of range";
        return false;
    }
    if (L.size()==0 or L.size()!=R.size())
    {
        qDebug() << "Scale::insert of key" << key <<": Waveform sizes"
                 << L.size() << R.size() << "inconsistent";
        return false;
    }
    if (mWaves.size() != N) // this should never happen
    {
        qDebug() << "Scale::insert: mWaveForm size =" << mWaves.size() << "invalid";
        return false;
    }
    if (L.size() > 1<<20)
    {
        qDebug() << "Scale::insert: ERROR: mWaveForm exceeds maximal size 2^20";
        return false;
    }
    return mWaves[key].insert(L,R,release,amplification);
}

//-----------------------------------------------------------------------------
//                        Write scale data to device
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Write scale to device
/// \details This function writes the entire scale to a QIODevice, e.g. to a
/// file. It writes a tag (named ctag) followed by all 128 wave objects.
/// \param iodevice : QIODevice to which the data is written
/// \param voice : Pointer back to the voice, needed to report progress
/// \return
///////////////////////////////////////////////////////////////////////////////

bool Scale::write(QIODevice &iodevice, Voice* voice)
{
    InstrumentFileHandler::write(iodevice,ctag);
    // Count nonzero entries
    int cnt=0;
    for (Wave &waveform : mWaves) if (waveform.waveFormExists()) cnt++;
    InstrumentFileHandler::write(iodevice,cnt);

    // Write file
    double progress=0;
    for (Wave &waveform : mWaves)
    {
        if (cnt) progress += 1.0/cnt; else progress=1;
        if (voice) voice->setProgress(QVariant(progress));
        if (not waveform.write(iodevice)) return false;
        if (mCancellationRequested) break;
    }
    return true;
}


//-----------------------------------------------------------------------------
//                        Read scale data from device
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Read data of a scale from a device.
/// \param iodevice : Reference to the QIODevice, opened for reading
/// \param voice
/// \return
////////////////////////////////////////////////////////////////////////////////

bool Scale::read (QIODevice &iodevice, Voice* voice)
{
    mCancellationRequested = false;
    int tag=0, cnt=0;
    InstrumentFileHandler::read (iodevice,tag);
    InstrumentFileHandler::read (iodevice,cnt);
    if (tag != ctag)
    {
        qWarning() << "Scale::read: invalid tag.";
        return false;
    }

    double progress = 0;
    for (Wave &waveform : mWaves)
    {
        if (not waveform.read(iodevice)) return false;
        if (waveform.waveFormExists() and cnt>0) progress += 1.0/cnt;
        if (voice) emit voice->setProgress(QVariant(progress));
        if (mCancellationRequested) break;
    }
    return not mCancellationRequested;
}


//-----------------------------------------------------------------------------
//                      Print information about this scale
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Scale::printInfo
///////////////////////////////////////////////////////////////////////////////

void Scale::printInfo()
{
    qDebug() << "   Scale with" << mWaves.size() << "waveforms.";
    for (int n=0; n<128; ++n) mWaves[n].printInfo(n);
}


