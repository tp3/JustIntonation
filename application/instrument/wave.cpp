/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//            Wave: Class holding the sampled sound for one key
//=============================================================================

#include "wave.h"

#include <QFile>
#include <QDebug>
#include <QThread>
#include <QtGlobal>


#include "instrumentfilehandler.h"
#include "voice.h"

#define STATUS  if (Voice::mVerbosity >= 4) qDebug() << "Wave:"
#define MESSAGE if (Voice::mVerbosity >= 3) qDebug() << "Wave:"
#define WARNING if (Voice::mVerbosity >= 2) qWarning()<<"Wave: WARNING:"
#define ERROR   if (Voice::mVerbosity >= 1) qCritical() << "Wave: ERROR:"


//-----------------------------------------------------------------------------
//                               Constructor
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// Constructor, resetting member variables
///////////////////////////////////////////////////////////////////////////////

Wave::Wave()
    : mRepetitionIndex(0)
    , mRepetitionFactor(1)
    , mSustainSample()
    , mReleaseSample()
    , mSustainShift(0)
    , mReleaseShift(0)
    , mEnvelope()
{}


//-----------------------------------------------------------------------------
//                    Compute a synthetic triangular wave
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Compute a synthetic triangular wave
/// \param frequency : Frequency in Hz
/// \param samplerate : Sampling rate
/// \param stereo : Parameter contolling the stereo position (0...1)
///////////////////////////////////////////////////////////////////////////////

void Wave::computeTriangularWave (double frequency, int samplerate, double stereo)
{
    if (samplerate==0) return;
    const int N = 1<<20;
    mReleaseSample.clear();
    mSustainSample.resize(N);
    double phaseleft = 2*stereo;
    double phaseright = - 2*stereo;
    double amplitude=0;
    double increment = frequency / samplerate;

    for (int i=0; i<N/2; ++i)
    {
        phaseleft += increment;  while (phaseleft>1) phaseleft--;
        phaseright += increment; while (phaseright>1) phaseright--;
        amplitude = 32768.0*std::min(500*i,N/2-i)/N*2;
        mSustainSample[2*i] =   amplitude*(phaseleft < 0.5  ?  0.22 - phaseleft : phaseleft-0.72);
        mSustainSample[2*i+1] = amplitude*(phaseright < 0.5 ? 0.22 - phaseright : phaseright-0.72);
    }
    mSustainShift=mReleaseShift=8;
    computeEnvelope(samplerate);
}


//-----------------------------------------------------------------------------
//                  Insert waveform (called from importer)
//-----------------------------------------------------------------------------

bool Wave::insert(const QVector<qint32> &L, const QVector<qint32> &R,
                  const bool release, const double amplitude)
{
    // Find the maximal amplitude
    qint32 maximalAmplitude = 0;
    for (auto &a : L) if (abs(a)>maximalAmplitude) maximalAmplitude=abs(a);
    for (auto &a : R) if (abs(a)>maximalAmplitude) maximalAmplitude=abs(a);

    // Count by how many bits the max amplitude exceeds a 16bit integer
    int shift = 0;
    while (maximalAmplitude & 0x7FFF8000) { shift++; maximalAmplitude >>= 1; }
    int divisor = (1<<shift);

    // Specify an amplitude shift for playback
    int ampshift = 0;
    if (amplitude > 1) ampshift = round(log(amplitude)/log(2.0));
    else divisor <<= static_cast<int>(round(-log(amplitude)/log(2.0)));

    // Make sure that there are no more than 2^20 frames
    int N=L.size();
    if (N > (1<<20))
    {
        qDebug() << "Wave::insert: Wave size" << L.size() << "exceeds 2^20";
        return false;
    }

    if (release)
    {
        mReleaseSample.resize(2*N);
        mReleaseShift = shift + ampshift;
        for (int i=0; i<N; i++)
        {
            mReleaseSample[2*i]   = static_cast<qint16>(L[i]/divisor);
            mReleaseSample[2*i+1] = static_cast<qint16>(R[i]/divisor);
        }
    }
    else // if wave
    {
        mSustainSample.resize(2*N);
        mSustainShift = shift + ampshift;
        for (int i=0; i<N; i++)
        {
            mSustainSample[2*i]   = static_cast<quint16>(L[i]/divisor);
            mSustainSample[2*i+1] = static_cast<quint16>(R[i]/divisor);
        }
        if (N == (1<<20))
        {
            STATUS << "Compute envelope and perform automatic cyclic morphing";
            computeEnvelope(44100); // real sample rate required
            automaticCyclicMorphing();
        }
    }
    return true;
}

//-----------------------------------------------------------------------------
//                            Write a wave to disk
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Write a PCM wave to disk (QIODevice)
/// \param iodevice : Reference to the QIODevice opened for writing
/// \return True if successful
///////////////////////////////////////////////////////////////////////////////

bool Wave::write(QIODevice &iodevice)
{
    STATUS << "Writing PCM to QIODevice";
    // Write tag and repetition indices
    const int tag=0x11111111;
    if (not InstrumentFileHandler::write(iodevice,tag)) return false;
    InstrumentFileHandler::write(iodevice,mRepetitionIndex);
    InstrumentFileHandler::write(iodevice,mRepetitionFactor);

    // Encapsulate writing process in a local function:
    auto writeWaveform = [&iodevice] (QVector<qint16> &wave)
    {
        InstrumentFileHandler::write(iodevice,wave.size());
        if (wave.size()>0)
        {
            QByteArray data = QByteArray::fromRawData(
                    reinterpret_cast<const char*>(wave.constData()),
                    sizeof(qint16) * wave.size());
            // Compress data before writing
            QByteArray compressed = qCompress(data,9);
            InstrumentFileHandler::write(iodevice,compressed.size());
            STATUS << "Compressing" << 2*wave.size() << "->" << compressed.size();
            int bytes = static_cast<int>(iodevice.write(compressed));
            if (bytes != compressed.size())
            {
                ERROR << "Could not write compressed data";
                return false;
            }
        }
        return true;
    };

    if (not writeWaveform(mSustainSample)) return false;
    InstrumentFileHandler::write(iodevice,mSustainShift);
    if (not writeWaveform(mReleaseSample)) return false;
    InstrumentFileHandler::write(iodevice,mReleaseShift);
    return true;
}


//-----------------------------------------------------------------------------
//                           Read a wave from disk
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Read a wave from disk
/// \details This function reads the member functions of the class Wave
/// from the given QIODevice. The reading is aborted as soon as the
/// QThread::isInterruptionRequested() flag is turned on.
/// \param iodevice : Reference to the QIODevice opened for reading
/// \return True if successful
///////////////////////////////////////////////////////////////////////////////

bool Wave::read(QIODevice &iodevice)
{
    // First verify tag and read the repetition indices
    int tag=0;
    if (not InstrumentFileHandler::read(iodevice,tag) or tag!=0x11111111) return false;
    InstrumentFileHandler::read(iodevice,mRepetitionIndex);
    InstrumentFileHandler::read(iodevice,mRepetitionFactor);

    // Reading is encapsulted in the following local function
    auto readWaveform = [&iodevice] (QVector<qint16> &wave)
    {
        int uncompressedSize=0, compressedSize=0;
        InstrumentFileHandler::read(iodevice,uncompressedSize);
        if (uncompressedSize==0) wave.clear();
        else
        {
            InstrumentFileHandler::read(iodevice,compressedSize);
            if (QThread::currentThread()->isInterruptionRequested()) return false;
            QByteArray array = iodevice.read(compressedSize);
            QByteArray uncompressed = qUncompress(array);
            if (uncompressed.size() != 2*uncompressedSize)
            {
                WARNING << "WaveForm::read: incompatible sizes. File corrupted?";
                return false;
            }
            wave.clear();
            wave.resize(uncompressedSize);
            memcpy(&wave[0], uncompressed.constData(), uncompressed.size());
            if (QThread::currentThread()->isInterruptionRequested()) return false;
            STATUS << "Uncompressing" << compressedSize << array.size()
                   << "->" << 2*uncompressedSize << uncompressed.size();
        }
        return true;
    };

    if (not readWaveform(mSustainSample)) return false;
    InstrumentFileHandler::read(iodevice,mSustainShift);
    if (not readWaveform(mReleaseSample)) return false;
    InstrumentFileHandler::read(iodevice,mReleaseShift);

    computeEnvelope(44100); // sample rate needed ******************************* ********** !!!!!!!!!!!!!!!!!!!!!

    return true;
}



//-----------------------------------------------------------------------------
//              Write a short summary of the wave data to qDebug()
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Debugging function: Write a short summary of the wave data to qDebug()
/// \param keynumber : Number of the key to be displayed
///////////////////////////////////////////////////////////////////////////////

void Wave::printInfo(int keynumber)
{
    if (mSustainSample.size()==0) return;
    qDebug() << "       Key" << keynumber << ": Waveform containing"
             << mSustainSample.size()/2 << "frames" << "Repetition data"
             << mRepetitionIndex << mRepetitionFactor;
}


//-----------------------------------------------------------------------------
//                Determine the envelope of the sustain wave
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Determine the envelope of the sustain wave
/// \param samplerate
///////////////////////////////////////////////////////////////////////////////

void Wave::computeEnvelope(int samplerate)
{
    const double damping = 0.0007;
    const quint32 mask = envelopeWidth-1;
    mEnvelope.resize((mSustainSample.size()-1)/envelopeWidth+1);
    double slidingPower = 0;
    int equilibrationsize = qMin(mSustainSample.size(),samplerate/3);
    for (int index = 0; index < equilibrationsize; ++index)
        slidingPower = slidingPower*(1-damping) + mSustainSample[index]*mSustainSample[index]*damping;
    for (int index = 0; index < mSustainSample.size(); ++index)
    {
        slidingPower = slidingPower*(1-damping) + mSustainSample[index]*mSustainSample[index]*damping;
        if ((index & mask) == mask) mEnvelope[index/envelopeWidth] = sqrt(slidingPower);
    }
    mEnvelope[mEnvelope.size()-1] = sqrt(slidingPower);
}
