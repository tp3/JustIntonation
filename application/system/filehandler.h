/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

/*****************************************************************************
 * Copyright 2016-2017 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//                              Open File Dialog
//=============================================================================


#ifndef FILEHANDLER_H
#define FILEHANDLER_H

#include "system/log.h"


////////////////////////////////////////////////////////////////////////////////
/// \brief Module for a file-open dialog
////////////////////////////////////////////////////////////////////////////////

class FileHandler : public QObject, public Log
{
    Q_OBJECT
public:
    FileHandler();
public slots:
    void openMidiFileDialog(int width, int height);
    void saveData (QString data1,QString data2, QString data3);
    void loadData ();

signals:
    void openMidiFile (QString fileName, bool autostart);
    void loadedData (QVariant data1, QVariant data2, QVariant data3);

private:
    const QString tag = "JustIntonation data file";
};

#endif // FILEHANDLER_H
