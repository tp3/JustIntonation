/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//                         Namespace System Tools
//=============================================================================

#include <QString>

namespace SystemTools
{

//-----------------------------------------------------------------------------
//                             Get thread name or id
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Helper function for debugging: Get the current thread id and time
/// \return QString holding the name of the thread in a readable format
/// \note This function works only on Linux systems, it is inactive on other
/// platforms
///////////////////////////////////////////////////////////////////////////////

QString getThreadName()
{
    QString s;
#if defined(Q_OS_LINUX) && !defined(Q_OS_ANDROID)
    unsigned int id = (unsigned int)pthread_self();
    char buffer[100];
    pthread_getname_np(pthread_self(),buffer,100);
    s += "id = " + QString::number(id) + " (" + QString(buffer) + ")";
#else
    s = QString("(thread name not available on this platform)");
#endif
    return s;
}


//-----------------------------------------------------------------------------
//                        Specify the current thread name
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Helper function for debugging: set the current thread name
/// \param threadname : QString holding the name of the thread
/// \note This function works only on Linux systems, it is inactive on other
/// platforms
///////////////////////////////////////////////////////////////////////////////

void setThreadName (QString threadname)
{
#if defined(Q_OS_LINUX) && !defined(Q_OS_ANDROID)
    pthread_setname_np (pthread_self(), threadname.toStdString().c_str());
#else
    (void)threadname;
#endif
}



}
