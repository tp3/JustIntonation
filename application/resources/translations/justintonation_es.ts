<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_ES">
<context>
    <name>About</name>
    <message>
        <location filename="../qml/About.qml" line="95"/>
        <source>Theoretical Physics III</source>
        <translation>Física Teórica III</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="96"/>
        <source>Faculty for Physics and Astronomy</source>
        <translation>Facultad de Física y Astronomía</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="97"/>
        <source>University of</source>
        <translation>Universidad de</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="97"/>
        <source>, Germany</source>
        <translation>, Alemania</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="117"/>
        <source>Version</source>
        <translation>Versión</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="165"/>
        <source>is free software licensed under GNU GPLv3. The source code can be downloaded from Gitlab:</source>
        <translation>es un software libre con licencia bajo GNU GPLv3. El código fuente se puede descargar de Gitlab:</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="208"/>
        <source>This software is based on the following open-source third-party components:</source>
        <translation>Este software se basa en los siguientes componentes de código abierto de terceros:</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="275"/>
        <source>We thank all those who have contributed to the project:</source>
        <translation>Agradecemos a todos aquellos que han contribuido al proyecto:</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="278"/>
        <source>Translations: </source>
        <translation>Traducciones: </translation>
    </message>
</context>
<context>
    <name>Application</name>
    <message>
        <location filename="../../application.cpp" line="538"/>
        <source>Grand Piano</source>
        <translation>Piano de cola</translation>
    </message>
    <message>
        <location filename="../../application.cpp" line="539"/>
        <source>Organ</source>
        <translation>Organo</translation>
    </message>
    <message>
        <location filename="../../application.cpp" line="540"/>
        <source>Harpsichord</source>
        <translation>Clave</translation>
    </message>
    <message>
        <location filename="../../application.cpp" line="541"/>
        <source>Unknown Instrument</source>
        <translation>Instrumento desconocido</translation>
    </message>
    <message>
        <location filename="../../application.cpp" line="543"/>
        <source>Artificial sound</source>
        <translation>Sonido artificial</translation>
    </message>
    <message>
        <location filename="../../application.cpp" line="544"/>
        <source>External device</source>
        <translation>Dispositivo externo</translation>
    </message>
    <message>
        <location filename="../../application.cpp" line="812"/>
        <source>[Choose output device]</source>
        <translation>[Elegir dispositivo]</translation>
    </message>
    <message>
        <location filename="../../application.cpp" line="858"/>
        <source>No device connected</source>
        <translation>Ningún dispositivo conectado</translation>
    </message>
</context>
<context>
    <name>AudioOutput</name>
    <message>
        <location filename="../qml/AudioOutput.qml" line="76"/>
        <source>Sample rate:</source>
        <translation>Frecuencia de muestreo:</translation>
    </message>
    <message>
        <location filename="../qml/AudioOutput.qml" line="105"/>
        <source>Buffer size:</source>
        <translation>Tamaño del búfer:</translation>
    </message>
    <message>
        <location filename="../qml/AudioOutput.qml" line="120"/>
        <source>Show more help on audio settings</source>
        <translation>Mostrar más ayuda sobre la configuración de audio</translation>
    </message>
    <message>
        <location filename="../qml/AudioOutput.qml" line="148"/>
        <source>Packet size:</source>
        <translation>Tamaño del paquete:</translation>
    </message>
    <message>
        <location filename="../qml/AudioOutput.qml" line="179"/>
        <source>Reset</source>
        <translation>Reiniciar</translation>
    </message>
    <message>
        <location filename="../qml/AudioOutput.qml" line="182"/>
        <source>Reset audio settings to default values</source>
        <translation>Restablecer la configuración de audio a los valores predeterminados</translation>
    </message>
    <message>
        <location filename="../qml/AudioOutput.qml" line="189"/>
        <source>Apply Changes</source>
        <translation>Aplicar cambios</translation>
    </message>
    <message>
        <location filename="../qml/AudioOutput.qml" line="190"/>
        <source>Apply the audio settings specified in this panel</source>
        <translation>Aplique los ajustes de audio especificados en este panel</translation>
    </message>
</context>
<context>
    <name>Dialog</name>
    <message>
        <location filename="../qml/Dialog.qml" line="85"/>
        <source>Yes</source>
        <translation>Sí</translation>
    </message>
    <message>
        <location filename="../qml/Dialog.qml" line="85"/>
        <source>OK</source>
        <translation>De acuerdo</translation>
    </message>
    <message>
        <location filename="../qml/Dialog.qml" line="95"/>
        <source>No</source>
        <translation>No</translation>
    </message>
</context>
<context>
    <name>Examples</name>
    <message>
        <location filename="../qml/Examples.qml" line="45"/>
        <source>Example </source>
        <translation>Ejemplo </translation>
    </message>
    <message>
        <location filename="../qml/Examples.qml" line="85"/>
        <source>Play previous MIDI example</source>
        <translation>Reproducir el ejemplo anterior de MIDI</translation>
    </message>
    <message>
        <location filename="../qml/Examples.qml" line="92"/>
        <source>Info</source>
        <translation>Información</translation>
    </message>
    <message>
        <location filename="../qml/Examples.qml" line="95"/>
        <source>Show copyright information about the MIDI file</source>
        <translation>Mostrar información de copyright sobre el archivo MIDI</translation>
    </message>
    <message>
        <location filename="../qml/Examples.qml" line="104"/>
        <source>Play next MIDI example</source>
        <translation>Reproducir el siguiente ejemplo de MIDI</translation>
    </message>
</context>
<context>
    <name>Expert</name>
    <message>
        <location filename="../qml/Expert.qml" line="85"/>
        <source>Go back to Simple Mode</source>
        <translation>Volver a Modo Simple</translation>
    </message>
    <message>
        <location filename="../qml/Expert.qml" line="89"/>
        <source>Help and General Information</source>
        <translation>Ayuda e Información General</translation>
    </message>
    <message>
        <location filename="../qml/Expert.qml" line="94"/>
        <source>Modify Preferences</source>
        <translation>Modificar preferencias</translation>
    </message>
    <message>
        <location filename="../qml/Expert.qml" line="99"/>
        <source>Play Music on a Touchscreen Keyboard</source>
        <translation>Reproducir música en un teclado con pantalla táctil</translation>
    </message>
    <message>
        <location filename="../qml/Expert.qml" line="104"/>
        <source>Show Logfile</source>
        <translation>Mostrar archivo de registro</translation>
    </message>
    <message>
        <location filename="../qml/Expert.qml" line="111"/>
        <source>Expert Mode</source>
        <translation>Modo experto</translation>
    </message>
    <message>
        <location filename="../qml/Expert.qml" line="168"/>
        <source>Midi Player</source>
        <translation>Reproductor de Midi</translation>
    </message>
    <message>
        <location filename="../qml/Expert.qml" line="180"/>
        <source>Midi Examples</source>
        <translation>Ejemplos de Midi</translation>
    </message>
    <message>
        <location filename="../qml/Expert.qml" line="193"/>
        <source>Instrument</source>
        <translation>Instrumento</translation>
    </message>
    <message>
        <location filename="../qml/Expert.qml" line="231"/>
        <source>Frequency:</source>
        <translation>Frecuencia:</translation>
    </message>
    <message>
        <location filename="../qml/Expert.qml" line="267"/>
        <source>Reset concert pitch to the standard value of 440 Hz</source>
        <translation>Restablecer el pitch de los conciertos al valor estándar de 440 Hz</translation>
    </message>
    <message>
        <location filename="../qml/Expert.qml" line="304"/>
        <source>Tuning Mode</source>
        <translation>Mode de afinación</translation>
    </message>
    <message>
        <location filename="../qml/Expert.qml" line="314"/>
        <source>Temperament</source>
        <translation>Temperamento</translation>
    </message>
</context>
<context>
    <name>FileHandler</name>
    <message>
        <location filename="../../system/filehandler.cpp" line="60"/>
        <source>Please choose a Midi file</source>
        <translation>Elija un archivo Midi</translation>
    </message>
    <message>
        <location filename="../../system/filehandler.cpp" line="60"/>
        <source>Midi files (*.mid *.midi *.MID)</source>
        <translation>Archivos Midi (*.mid *.midi *.MID)</translation>
    </message>
    <message>
        <location filename="../../system/filehandler.cpp" line="80"/>
        <source>Save custom temperament</source>
        <translation>Guardar temperamento personalizado</translation>
    </message>
    <message>
        <location filename="../../system/filehandler.cpp" line="81"/>
        <location filename="../../system/filehandler.cpp" line="116"/>
        <source>Temperament files</source>
        <translation>Archivos de temperamento</translation>
    </message>
    <message>
        <location filename="../../system/filehandler.cpp" line="81"/>
        <location filename="../../system/filehandler.cpp" line="116"/>
        <source>All files</source>
        <translation>Todos los archivos</translation>
    </message>
    <message>
        <location filename="../../system/filehandler.cpp" line="89"/>
        <source>Could not write to disk.</source>
        <translation>No se pudo escribir en el disco.</translation>
    </message>
    <message>
        <location filename="../../system/filehandler.cpp" line="115"/>
        <source>Load custom temperament</source>
        <translation>Cargar temperamento personalizado</translation>
    </message>
    <message>
        <location filename="../../system/filehandler.cpp" line="123"/>
        <source>Could not read from disk.</source>
        <translation>No se pudo cargar desde el disco.</translation>
    </message>
    <message>
        <location filename="../../system/filehandler.cpp" line="132"/>
        <source>Reading Error</source>
        <translation>Error al cargar</translation>
    </message>
    <message>
        <location filename="../../system/filehandler.cpp" line="132"/>
        <source>This is not a data file generated by JustIntonation</source>
        <translation>Este no es un archivo de datos generado por JustIntonation</translation>
    </message>
</context>
<context>
    <name>GettingStarted</name>
    <message>
        <location filename="../qml/GettingStarted.qml" line="28"/>
        <source>Getting started</source>
        <translation>Primeros pasos</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="63"/>
        <source>Welcome to &quot;Just Intonation&quot;.</source>
        <translation>Bienvenido a  &quot;Just Intonation&quot; (Temperamento justo).</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="72"/>
        <source>With this application you can experience how it would be to hear and play Music in just intonation, independent of scale!</source>
        <translation>Con esta aplicación puedes experimentar cómo sería escuchar y tocar música en temperamento justo, independiente de la escala!</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="73"/>
        <source>System Requirements</source>
        <translation>Requisitos del sistema</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="74"/>
        <source>The application runs on most desktop computers and on sufficiently powerful tablets and smartphones.</source>
        <translation>La aplicación se ejecuta en la mayoría de los ordenadores de sobremesa y en tabletas y smartphones suficientemente potentes.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="75"/>
        <source>It needs about 300MB of temporary memory (RAM).</source>
        <translation>Necesita alrededor de 300 MB de memoria temporal (RAM).</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="76"/>
        <source>In addition, if you want to download samples of realistic musical instruments, approximately 600MB of permanent storage (disk space) is needed.</source>
        <translation>Además, si desea descargar muestras de instrumentos musicales realistas, se necesitan aproximadamente 600 MB de almacenamiento permanente (espacio en disco).</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="77"/>
        <source>Download Realistic Instruments</source>
        <translation>Descargar instrumentos  realistas</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="78"/>
        <source>On startup the application will ask you for permission to download samples of real instruments from our website.</source>
        <translation>Al arrancar la aplicación le pedirá permiso para descargar muestras de instrumentos reales de nuestro sitio web.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="79"/>
        <source>Depending on the speed of your internet connection this may take some time.</source>
        <translation>Dependiendo de la velocidad de su conexión a Internet esto puede tomar algún tiempo.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="80"/>
        <source>During the download you can already start to use the application.</source>
        <translation>Durante la descarga ya puede comenzar a utilizar la aplicación.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="81"/>
        <source>Listen to Music</source>
        <translation>Escuchar música</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="82"/>
        <source>To hear music just tap the leftmost button with the note in the lower toolbar.</source>
        <translation>Para escuchar música solo toca el botón más a la izquierda con la nota en la barra de herramientas inferior.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="83"/>
        <source>Toggle between just intonation and equal temperament and listen to the difference.</source>
        <translation>Cambia entre la entonación justa y el temperamento igual y escucha la diferencia.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="84"/>
        <source>Listen to your own Music</source>
        <translation>Escucha tu propia música</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="85"/>
        <source>Connect a Midi keyboard to your device.</source>
        <translation>Conecte un teclado Midi a su dispositivo.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="86"/>
        <source>The application will recognize most Midi devices automatically.</source>
        <translation>La aplicación reconocerá la mayoría de los dispositivos Midi automáticamente.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="87"/>
        <source>Just play on your keyboard and hear yourself playing in just intonation.</source>
        <translation>Simplemente toca en tu teclado y escúchate a ti mismo tocando solo en temperamento justo.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="88"/>
        <source>Midi keyboards can also be connected to mobile devices using a special adapter cable (see user manual).</source>
        <translation>Los teclados Midi también pueden conectarse a dispositivos móviles mediante un cable adaptador especial (consulte el manual del usuario).</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="89"/>
        <source>Expert Mode</source>
        <translation>Modo experto</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="90"/>
        <source>Tap the rightmost button in the lower toolbar to enter the Expert Mode.</source>
        <translation>Toque el botón más a la derecha en la barra de herramientas inferior para entrar en el modo experto.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="91"/>
        <source>Here you can choose various temperaments.</source>
        <translation>Aquí puede elegir varios temperamentos.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="92"/>
        <source>Moreover, you can control a large variety of parameters.</source>
        <translation>Además, puede controlar una gran variedad de parámetros.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="93"/>
        <source>It is also possible to monitor the pitch drift while you are playing.</source>
        <translation>También es posible monitorear la desviación de tono mientras está tocando.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="94"/>
        <source>More Information</source>
        <translation>Más información</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="95"/>
        <source>You want to learn more about Just Intonation and the history of temperaments?</source>
        <translation>¿Quieres saber más sobre temperamento justo y la historia de los temperamentos?</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="96"/>
        <source>You would like to know more about the features of the application and how it works internally?</source>
        <translation>¿Desea saber más sobre las características de la aplicación y cómo funciona internamente?</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="97"/>
        <source>Please download the User Manual and visit our website for more detailed information.</source>
        <translation>Descargue el Manual del usuario y visite nuestro sitio web para obtener información más detallada (en inglés).</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="98"/>
        <source>Thank you for your interest in Just Intonation.</source>
        <translation>Gracias por su interés en &quot;Just Intonation&quot;.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="112"/>
        <source>Download User Manual</source>
        <translation>Descargar manual del usuario</translation>
    </message>
</context>
<context>
    <name>Instrument</name>
    <message>
        <location filename="../../instrument/instrument.cpp" line="80"/>
        <source>Generating artificial sound.</source>
        <translation>Generación de sonido artificial.</translation>
    </message>
    <message>
        <location filename="../../instrument/instrument.cpp" line="96"/>
        <source>Loading instrument. Please wait...</source>
        <translation>Cargando el instrumento. Por favor espera...</translation>
    </message>
</context>
<context>
    <name>Keyboard</name>
    <message>
        <location filename="../qml/Keyboard.qml" line="70"/>
        <source>Keyboard</source>
        <translation>Teclado</translation>
    </message>
    <message>
        <location filename="../qml/Keyboard.qml" line="134"/>
        <source>Equal</source>
        <translation>Igual</translation>
    </message>
    <message>
        <location filename="../qml/Keyboard.qml" line="135"/>
        <source>Just</source>
        <translation>Justo</translation>
    </message>
    <message>
        <location filename="../qml/Keyboard.qml" line="135"/>
        <source>Nonequal</source>
        <translation>No igual</translation>
    </message>
    <message>
        <location filename="../qml/Keyboard.qml" line="151"/>
        <source>Mouse</source>
        <translation>Ratón</translation>
    </message>
    <message>
        <location filename="../qml/Keyboard.qml" line="152"/>
        <source>Touchscreen</source>
        <translation>Pantalla táctil</translation>
    </message>
    <message>
        <location filename="../qml/Keyboard.qml" line="163"/>
        <source>Single tone</source>
        <translation>Tono único</translation>
    </message>
    <message>
        <location filename="../qml/Keyboard.qml" line="164"/>
        <source>Triad</source>
        <translation>Tríada</translation>
    </message>
</context>
<context>
    <name>LearnMore</name>
    <message>
        <location filename="../qml/LearnMore.qml" line="29"/>
        <source>Learn more</source>
        <translation>Aprende más</translation>
    </message>
    <message>
        <location filename="../qml/LearnMore.qml" line="51"/>
        <source>Basic Help for Getting Started</source>
        <translation>Ayuda básica para empezar</translation>
    </message>
    <message>
        <location filename="../qml/LearnMore.qml" line="52"/>
        <source>Getting started</source>
        <translation>Primeros pasos</translation>
    </message>
    <message>
        <location filename="../qml/LearnMore.qml" line="60"/>
        <source>Download User Manual from the Internet</source>
        <translation>Descargar el manual del usuario desde Internet</translation>
    </message>
    <message>
        <location filename="../qml/LearnMore.qml" line="61"/>
        <source>Download manual</source>
        <translation>Descargar manual</translation>
    </message>
    <message>
        <location filename="../qml/LearnMore.qml" line="69"/>
        <source>Visit Project Website on the Internet</source>
        <translation>Visite el sitio web del proyecto en Internet</translation>
    </message>
    <message>
        <location filename="../qml/LearnMore.qml" line="71"/>
        <source>Visit website</source>
        <translation>Visita el sitio web</translation>
    </message>
    <message>
        <location filename="../qml/LearnMore.qml" line="79"/>
        <source>Download Scientific Documentation describing Details of the Tuning Method</source>
        <translation>Descargue la documentación científica que describe los detalles del método de afinación</translation>
    </message>
    <message>
        <location filename="../qml/LearnMore.qml" line="81"/>
        <source>Tuning Method</source>
        <translation>Método de afinación</translation>
    </message>
    <message>
        <location filename="../qml/LearnMore.qml" line="90"/>
        <source>Get more Information about this Application</source>
        <translation>Obtenga más información sobre esta aplicación</translation>
    </message>
    <message>
        <location filename="../qml/LearnMore.qml" line="91"/>
        <source>About this App</source>
        <translation>Acerca de esta aplicación</translation>
    </message>
</context>
<context>
    <name>LogFile</name>
    <message>
        <location filename="../../system/logfile.cpp" line="77"/>
        <source>A#</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../system/logfile.cpp" line="77"/>
        <source>B</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Logfile</name>
    <message>
        <location filename="../qml/Logfile.qml" line="29"/>
        <source>Clear</source>
        <translation>Borrar</translation>
    </message>
    <message>
        <location filename="../qml/Logfile.qml" line="32"/>
        <source>Clear log file</source>
        <translation>Borrar archivo de registro</translation>
    </message>
    <message>
        <location filename="../qml/Logfile.qml" line="38"/>
        <source>Copy</source>
        <translation>Copia</translation>
    </message>
    <message>
        <location filename="../qml/Logfile.qml" line="39"/>
        <source>Copy log file to the clipboard</source>
        <translation>Copiar archivo de registro en el portapapeles</translation>
    </message>
    <message>
        <location filename="../qml/Logfile.qml" line="48"/>
        <source>Rewind: Go back to the beginning</source>
        <translation>Rebobinar: Regresar al principio</translation>
    </message>
    <message>
        <location filename="../qml/Logfile.qml" line="55"/>
        <source>Play / Pause</source>
        <translation>Reproducir / Pausar</translation>
    </message>
    <message>
        <location filename="../qml/Logfile.qml" line="66"/>
        <source>Close Logfile</source>
        <translation>Cerrar archivo de registro</translation>
    </message>
    <message>
        <location filename="../qml/Logfile.qml" line="70"/>
        <source>Logfile</source>
        <translation>Archivo de registro</translation>
    </message>
</context>
<context>
    <name>Main</name>
    <message>
        <location filename="../qml/Main.qml" line="55"/>
        <source>https://www.gnu.org/licenses/gpl.html</source>
        <translation>https://www.gnu.org/licenses/gpl-3.0.es.html</translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="58"/>
        <source>http://www.just-intonation.org</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="170"/>
        <source>manual_en.pdf</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="257"/>
        <source>Equal Temperament</source>
        <translation>Temperamento igual</translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="258"/>
        <source>Just Intonation</source>
        <translation>Temperamento justo</translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="259"/>
        <source>Pythagorean tuning</source>
        <translation>Afinación pitagórica</translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="260"/>
        <source>1/4 Meantone</source>
        <translation>Temperamento mesotónico 1/4</translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="261"/>
        <source>Werckmeister III</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="262"/>
        <source>Silbermann 1/6</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="263"/>
        <source>Zarlino -2/7</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="264"/>
        <source>User-defined</source>
        <translation>Definido por el usuario</translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="392"/>
        <location filename="../qml/Main.qml" line="407"/>
        <source>Inactive</source>
        <translation>Inactivo</translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="413"/>
        <source>[Choose Audio Device]</source>
        <translation>[Seleccionar dispositivo de audio]</translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="415"/>
        <source>[Choose]</source>
        <translation>[Seleccionar]</translation>
    </message>
</context>
<context>
    <name>Midi</name>
    <message>
        <location filename="../../midi.cpp" line="415"/>
        <location filename="../../midi.cpp" line="449"/>
        <location filename="../../midi.cpp" line="505"/>
        <source>Inactive</source>
        <translation>Inactivo</translation>
    </message>
    <message>
        <location filename="../../midi.cpp" line="506"/>
        <source>[Choose Midi device]</source>
        <translation>[Seleccionar dispositivo Midi]</translation>
    </message>
</context>
<context>
    <name>MidiInput</name>
    <message>
        <location filename="../qml/MidiInput.qml" line="69"/>
        <source> Automatic Mode</source>
        <translation> Modo automatico</translation>
    </message>
    <message>
        <location filename="../qml/MidiInput.qml" line="97"/>
        <source>In the automatic mode new Midi devices are recognized automatically</source>
        <translation>En el modo automático, los nuevos dispositivos Midi se reconocen automáticamente</translation>
    </message>
</context>
<context>
    <name>MidiOutput</name>
    <message>
        <location filename="../qml/MidiOutput.qml" line="48"/>
        <source>Channel:</source>
        <translation>Canal:</translation>
    </message>
    <message>
        <location filename="../qml/MidiOutput.qml" line="97"/>
        <source> Automatic Mode</source>
        <translation> Modo automatico</translation>
    </message>
    <message>
        <location filename="../qml/MidiOutput.qml" line="126"/>
        <source>In the automatic mode new Midi devices are recognized automatically</source>
        <translation>En el modo automático, los nuevos dispositivos Midi se reconocen automáticamente</translation>
    </message>
</context>
<context>
    <name>MidiPlayer</name>
    <message>
        <location filename="../qml/MidiPlayer.qml" line="109"/>
        <source>Rewind: Go back to the beginning</source>
        <translation>Rebobinar: Regresar al principio</translation>
    </message>
    <message>
        <location filename="../qml/MidiPlayer.qml" line="117"/>
        <source>Play / Pause</source>
        <translation>Reproducir / Pausar</translation>
    </message>
    <message>
        <location filename="../qml/MidiPlayer.qml" line="127"/>
        <source>Repeat mode</source>
        <translation>Modo de repetición</translation>
    </message>
    <message>
        <location filename="../qml/MidiPlayer.qml" line="135"/>
        <source>Open a Midi file</source>
        <translation>Abrir un archivo Midi</translation>
    </message>
</context>
<context>
    <name>NavigationManager</name>
    <message>
        <location filename="../qml/NavigationManager.qml" line="152"/>
        <source>Reset settings</source>
        <translation>Restablecer configuración</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="153"/>
        <source>Do you want to reset all settings?</source>
        <translation>¿Desea restablecer todos los ajustes?</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="154"/>
        <source>All changes that you made will be lost and the settings will be reset to default values.</source>
        <translation>Se perderán todos los cambios realizados y los valores predeterminados se restablecerán.</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="164"/>
        <source>Help on choosing the audio parameters</source>
        <translation>Ayuda para elegir los parámetros de audio</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="165"/>
        <source>The sample rate has to be 44100.</source>
        <translation>La tasa de muestreo debe ser 44100.</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="165"/>
        <source>The buffer size should be small.</source>
        <translation>El tamaño del búfer debe ser pequeño.</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="166"/>
        <source>Buffer size = 0 means to use the default value. Packet size and buffer size should be the same. The buffer size controls the size of the internal audio buffer of your device. If you hear crackling sounds this value should be increased. Try to find the lowest possible value. The packet size controls the number of frames generated by the application in a single packet. In case of latency problems try to reduce this value.</source>
        <translation>Buffer size = 0 significa usar el valor predeterminado. El tamaño del paquete y el tamaño del búfer deben ser los mismos. El tamaño del búfer controla el tamaño del búfer de audio interno del dispositivo. Si escucha sonidos crepitantes este valor debe ser aumentado. Trate de encontrar el valor más bajo posible. El tamaño del paquete controla el número de tramas generadas por la aplicación en un solo paquete. En caso de problemas de latencia intente reducir este valor.</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="173"/>
        <source>Confirm Download</source>
        <translation>Confirmar Descargar</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="174"/>
        <source>Would you like to hear realistic musical instruments?</source>
        <translation>¿Te gustaría escuchar instrumentos musicales realistas?</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="175"/>
        <source>In this case the application will silently download 600 MB of sampled sounds from the internet to your device.</source>
        <translation>En este caso, la aplicación descargará silenciosamente 600 MB de sonidos muestreados desde Internet a su dispositivo.</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="176"/>
        <source>If you allow the application to download samples from our server you can hear realistic instruments such as a piano, an organ, or a harpsichord. The samples are downloaded only once and saved locally. Please make sure that your device offers about 600 MB of free disk space. If you deny internet access, the application will only provide artificially generated sounds (triangular waves).</source>
        <translation>Si permite que la aplicación descargue muestras de nuestro servidor, podrá escuchar instrumentos realistas como un piano, un órgano o un clavicordio. Las muestras se descargan una sola vez y se guardan localmente. Asegúrese de que su dispositivo ofrece aproximadamente 600 MB de espacio libre en disco. Si niega el acceso a Internet, la aplicación sólo proporcionará sonidos generados artificialmente (ondas triangulares).</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="187"/>
        <source>No Internet Connection</source>
        <translation>Sin conexión a Internet</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="188"/>
        <source>Could not find a valid internet connection for downloading.</source>
        <translation>No se encontró una conexión a Internet válida para la descarga.</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="189"/>
        <source>Please use a fast internet connection (WLAN or wired).</source>
        <translation>Utilice una conexión rápida a Internet (WLAN o por cable).</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="190"/>
        <source>Retry</source>
        <translation>Reintentar</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="191"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
</context>
<context>
    <name>Preferences</name>
    <message>
        <location filename="../qml/Preferences.qml" line="35"/>
        <source>Preferences</source>
        <translation>Preferencias</translation>
    </message>
    <message>
        <location filename="../qml/Preferences.qml" line="86"/>
        <source>External Midi Keyboard</source>
        <translation>Teclado Midi Externo</translation>
    </message>
    <message>
        <location filename="../qml/Preferences.qml" line="96"/>
        <source>Audio Output Device</source>
        <translation>Dispositivo de salida de audio</translation>
    </message>
    <message>
        <location filename="../qml/Preferences.qml" line="119"/>
        <source>External Midi Output Device</source>
        <translation>Dispositivo de salida Midi externo</translation>
    </message>
    <message>
        <location filename="../qml/Preferences.qml" line="126"/>
        <source>System settings</source>
        <translation>Ajustes del sistema</translation>
    </message>
</context>
<context>
    <name>ProgressIndicator</name>
    <message>
        <location filename="../qml/ProgressIndicator.qml" line="132"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
</context>
<context>
    <name>RunGuard</name>
    <message>
        <location filename="../../modules/system/runguard.cpp" line="168"/>
        <source>Information</source>
        <translation>Información</translation>
    </message>
    <message>
        <location filename="../../modules/system/runguard.cpp" line="169"/>
        <source>Just Intonation is already running.</source>
        <translation>&quot;Just Intonation&quot; ya está en ejecución.</translation>
    </message>
    <message>
        <location filename="../../modules/system/runguard.cpp" line="170"/>
        <source>Please close the application before restarting.</source>
        <translation>Cierre la aplicación antes de reiniciar.</translation>
    </message>
</context>
<context>
    <name>Simple</name>
    <message>
        <location filename="../qml/Simple.qml" line="60"/>
        <source>Play a Midi demo or drop a Midi file here</source>
        <translation>Reproducir una demo Midi o arrastrar un archivo Midi aquí</translation>
    </message>
    <message>
        <location filename="../qml/Simple.qml" line="71"/>
        <source>Help and General Information</source>
        <translation>Ayuda e Información General</translation>
    </message>
    <message>
        <location filename="../qml/Simple.qml" line="76"/>
        <source>Play Music on a Touchscreen Keyboard</source>
        <translation>Reproducir música en un teclado con pantalla táctil</translation>
    </message>
    <message>
        <location filename="../qml/Simple.qml" line="81"/>
        <source>Modify Preferences</source>
        <translation>Modificar preferencias</translation>
    </message>
    <message>
        <location filename="../qml/Simple.qml" line="86"/>
        <source>Enter Expert Mode</source>
        <translation>Entrar en Modo Experto</translation>
    </message>
    <message>
        <location filename="../qml/Simple.qml" line="143"/>
        <source>Instrument:</source>
        <translation>Instrumento:</translation>
    </message>
    <message>
        <location filename="../qml/Simple.qml" line="197"/>
        <source>Equal Temperament</source>
        <translation>Temperamento igual</translation>
    </message>
    <message>
        <location filename="../qml/Simple.qml" line="198"/>
        <source>Just Intonation</source>
        <translation>Temperamento justo</translation>
    </message>
    <message>
        <location filename="../qml/Simple.qml" line="212"/>
        <source>Delayed Tuning</source>
        <translation>Afinación retrasada</translation>
    </message>
    <message>
        <location filename="../qml/Simple.qml" line="234"/>
        <source>Compensate Pitch Drift</source>
        <translation>Compensar la deriva del tono</translation>
    </message>
    <message>
        <location filename="../qml/Simple.qml" line="317"/>
        <source>Downloading Instruments</source>
        <translation>Descarga de instrumentos</translation>
    </message>
    <message>
        <location filename="../qml/Simple.qml" line="374"/>
        <source>Tap this button to hear a demo</source>
        <translation>Haga clic en este botón para escuchar una demo</translation>
    </message>
</context>
<context>
    <name>SystemSettings</name>
    <message>
        <location filename="../qml/SystemSettings.qml" line="40"/>
        <source>Welcome screen on startup</source>
        <translation>Pantalla de bienvenida al inicio</translation>
    </message>
    <message>
        <location filename="../qml/SystemSettings.qml" line="48"/>
        <source>Allow download of samples</source>
        <translation>Permitir la descarga de muestras</translation>
    </message>
    <message>
        <location filename="../qml/SystemSettings.qml" line="61"/>
        <source>Language</source>
        <translation>Idioma</translation>
    </message>
    <message>
        <location filename="../qml/SystemSettings.qml" line="80"/>
        <source>Forced instrument download</source>
        <translation>Forced descargar instrumento</translation>
    </message>
    <message>
        <location filename="../qml/SystemSettings.qml" line="102"/>
        <source>Restore default settings</source>
        <translation>Restaurar config. predeterminada</translation>
    </message>
    <message>
        <location filename="../qml/SystemSettings.qml" line="103"/>
        <source>Reset all settings and restart the application</source>
        <translation>Restablecer todos los ajustes y reiniciar la aplicación</translation>
    </message>
</context>
<context>
    <name>Temperament</name>
    <message>
        <location filename="../qml/Temperament.qml" line="36"/>
        <source>Semitone</source>
        <translation>Semitono</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="36"/>
        <source>Major Second</source>
        <translation>Segunda mayor</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="36"/>
        <source>Minor Third</source>
        <translation>Tercera menor</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="37"/>
        <source>Major Third</source>
        <translation>Tercera mayor</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="37"/>
        <source>Perfect Fourth</source>
        <translation>Cuarta justa</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="37"/>
        <source>Tritone</source>
        <translation>Tritono</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="38"/>
        <source>Perfect Fifth</source>
        <translation>Quinta justa</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="38"/>
        <source>Minor Sixth</source>
        <translation>Sexta menor</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="38"/>
        <source>Major Sixth</source>
        <translation>Sexta mayor</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="39"/>
        <source>Minor Seventh</source>
        <translation>Séptima menor</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="39"/>
        <source>Major Seventh</source>
        <translation>Séptima mayor</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="39"/>
        <source>Octave</source>
        <translation>Octava</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="70"/>
        <source>Static Tuning</source>
        <translation>Afinación estática</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="88"/>
        <source>A#</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="88"/>
        <source>B</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="106"/>
        <source>Subminor Seventh 7:4</source>
        <translation>Séptima natural 7:4</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="166"/>
        <source>Enter temperament name</source>
        <translation>Nombre del temperamento</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="258"/>
        <source>Reset weights</source>
        <translation>Reinicializar pesos</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="187"/>
        <source>Open a temperament file</source>
        <translation>Abrir un archivo de temperamento</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="206"/>
        <source>Save a temperament file</source>
        <translation>Guardar un archivo de temperamento</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="241"/>
        <source>Set all interval size deviations to zero (equal temperament)</source>
        <translation>Establezca todas las desviaciones de tamaño de intervalo a cero (temperamento igual)</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="259"/>
        <source>Restore recommended standard values for the weights</source>
        <translation>Restaurar los valores estándar recomendados para los pesos</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="288"/>
        <source>Interval:</source>
        <translation>Intervalo:</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="296"/>
        <source>Deviation:</source>
        <translation>Desviación:</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="306"/>
        <source>Weight:</source>
        <translation>Peso:</translation>
    </message>
</context>
<context>
    <name>TextHeader</name>
    <message>
        <location filename="../qml/TextHeader.qml" line="108"/>
        <source>Modify Preferences</source>
        <translation>Modificar preferencias</translation>
    </message>
    <message>
        <location filename="../qml/TextHeader.qml" line="120"/>
        <source>Close window</source>
        <translation>Cerrar ventana</translation>
    </message>
</context>
<context>
    <name>TranslatorSingleton</name>
    <message>
        <location filename="../../modules/system/translator.cpp" line="133"/>
        <source>Automatic</source>
        <translation>Automático</translation>
    </message>
</context>
<context>
    <name>TuningMode</name>
    <message>
        <location filename="../qml/TuningMode.qml" line="43"/>
        <source>Tuning delay</source>
        <translation>Afinación retrasada</translation>
    </message>
    <message>
        <location filename="../qml/TuningMode.qml" line="78"/>
        <source>Memory</source>
        <translation>Memoria</translation>
    </message>
    <message>
        <location filename="../qml/TuningMode.qml" line="115"/>
        <source>Drift correction</source>
        <translation>Corrección de la deriva</translation>
    </message>
    <message>
        <location filename="../qml/TuningMode.qml" line="215"/>
        <source>cent</source>
        <translation>cent</translation>
    </message>
    <message>
        <location filename="../qml/TuningMode.qml" line="279"/>
        <source>tempered</source>
        <translation>templado</translation>
    </message>
    <message>
        <location filename="../qml/TuningMode.qml" line="289"/>
        <source>just</source>
        <translation>justo</translation>
    </message>
</context>
<context>
    <name>Voice</name>
    <message>
        <location filename="../../instrument/voice.cpp" line="64"/>
        <source>Please wait...</source>
        <translation>Por favor espera...</translation>
    </message>
    <message>
        <location filename="../../instrument/voice.cpp" line="149"/>
        <source>Reading scale</source>
        <translation>Escala de lectura</translation>
    </message>
</context>
<context>
    <name>Welcome</name>
    <message>
        <location filename="../qml/Welcome.qml" line="52"/>
        <source>Hello!</source>
        <translation>Bienvenido!</translation>
    </message>
    <message>
        <location filename="../qml/Welcome.qml" line="64"/>
        <source>This application gives you the opportunity to play in just intonation independent of scale.</source>
        <translation>Esta aplicación te da la oportunidad de tocar en un temperamento justo independiente de la escala.</translation>
    </message>
    <message>
        <location filename="../qml/Welcome.qml" line="85"/>
        <source>Start</source>
        <translation>Iniciar</translation>
    </message>
    <message>
        <location filename="../qml/Welcome.qml" line="86"/>
        <source>Start Just Intonation</source>
        <translation>Iniciar la aplicación &quot;Just Intonation&quot;</translation>
    </message>
    <message>
        <location filename="../qml/Welcome.qml" line="97"/>
        <source>Learn more</source>
        <translation>Aprende más</translation>
    </message>
    <message>
        <location filename="../qml/Welcome.qml" line="98"/>
        <source>Visit our website to learn more about Just Intonation</source>
        <translation>Visite nuestro sitio web para aprender más sobre el temperamento justo</translation>
    </message>
</context>
</TS>
