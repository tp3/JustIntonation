/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

import QtQuick 2.3
import QtQuick.Extras 1.4
import QtQuick.Controls.Styles 1.4

// Simple mode view, providing a minimal user interface

Item {
    id: simple
    objectName: "simple"
    readonly property int vsize: window.fontsize*0.8 + parent.height*0.03
    readonly property int fontsize: window.fontsize*0.95

    //---------------------------------- Header -------------------------------

    Header {
        id: header
        anchors.left: parent.left
        anchors.right: parent.right
        height: scalefactor*0.3
    }


    //-------------------------------- Footer ---------------------------------

    Image {
        id: footer
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        height: parent.height * 0.19;
        source: "images/bottomtoolbar.jpg"

        Row {
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            height: scalefactor*0.17
            spacing: parent.width*0.05

            XToolButton {
                id: playbutton
                tooltip: qsTr("Play a Midi demo or drop a Midi file here")
                source: playing ? "icons/stop.png" : "icons/example.png"
                activated: playing
                onClicked: hearExample(0);
                DropArea {
                    anchors.fill: parent
                    onDropped: { playbutton.highlighted=false; if (drop.hasUrls) openFile(drop.urls[0]); }
                    onContainsDragChanged: { playbutton.highlighted = containsDrag; }
                }
            }
            XToolButton {
                tooltip: qsTr("Help and General Information")
                source: "icons/info.png"
                onClicked: stack.push(learnmore)
            }
            XToolButton {
                tooltip: qsTr("Play Music on a Touchscreen Keyboard")
                source: "icons/keyboard.png"
                onClicked: stack.push(keyboard)
            }
            XToolButton {
                tooltip: qsTr("Modify Preferences")
                source: "icons/settings.png"
                onClicked: stack.push(preferences)
            }
            XToolButton {
                tooltip: qsTr("Enter Expert Mode")
                source: "icons/expert.png"
                onClicked: stack.push(expert)
            }
        }
    }

    Rectangle {
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: footer.top
        height: header.height*0.04
        gradient: Gradient {
            GradientStop {
                position: 0
                color: "#ffffff"
            }
            GradientStop {
                position: 1
                color: "#000000"
            }
        }
    }

    Rectangle {
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        height: header.height*0.04
        gradient: Gradient {
            GradientStop {
                position: 0
                color: "#ffffff"
            }
            GradientStop {
                position: 1
                color: "#000000"
            }
        }
    }


    //-------------------------------- Middle ---------------------------------

    Item {
        id: instrumentRow
        objectName: "instrumentSelector1"
        anchors.top: header.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.topMargin: landscape ? 0.06*parent.height-0.01*parent.width : parent.height*0.16-0.1*parent.width
        anchors.leftMargin: parent.width*0.03
        anchors.rightMargin: parent.width*0.03
        height: vsize*1.2

        Text {
            id: text1
            text: qsTr("Instrument:")
            font.family: window.font
            anchors.left: parent.left
            anchors.verticalCenter: parent.verticalCenter
            font.pixelSize: fontsize
        }

        XCombo {
            id: instrumentselector
            fontsize: window.fontsize*0.9
            anchors.left: text1.right
            anchors.leftMargin: parent.width*0.04
            anchors.right: parent.right
            anchors.rightMargin: parent.height*0.05
            anchors.bottom: parent.bottom
            anchors.bottomMargin: parent.height*0.05
            anchors.top: parent.top
            anchors.topMargin: parent.height*0.05
            model: window.instrumentmodel

            property int index: window.instrumentindex
            property bool selected: false
            currentIndex: index
            onActivated: selected = true
            onCurrentIndexChanged: {
                if (selected) {
                    index = currentIndex;
                    window.selectInstrument(index)
                }
                else currentIndex = index;
                selected = false;
            }
            onIndexChanged: {
                if (index != window.instrumentindex) window.instrumentindex = index;
                if (index != currentIndex) currentIndex = index;
            }
        }
    }

    //--------------------------------- Middle --------------------------------

    Column {
        id: middleArea
        anchors.top: instrumentRow.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: footer.top
        anchors.topMargin: parent.height*(parent.height>parent.width ? 0.08 : 0.04)
        spacing: (middleArea.height-3*vsize)*0.3

        XSwitch {
            width: parent.width
            height: vsize*1
            fontsize: simple.fontsize
            leftText: qsTr("Equal Temperament")
            rightText: qsTr("Just Intonation")
            property int index : temperamentIndex
            value : index
            onValueChanged: {
                temperamentIndex = value
                setTemperament(value)
            }
            onIndexChanged: value = index
        }

        XCheckBox {
            id: delayed
            enabled: !downloading
            visible: !downloading
            text: qsTr("Delayed Tuning")
            height: vsize
            x: parent.width*0.5 - fontsize*5.5
            width: parent.width-vsize*0.05-x
            fontsize: simple.fontsize
            property bool delayOn: tuningDelay > 0.001
            checked: delayOn
            onDelayOnChanged: checked = delayOn
            onClicked: {
                if (tuningDelay < 0.001) tuningDelay = 1.5;
                else tuningDelay = 0;
            }
        }

        Item {
            height: 10*fontsize
        }

        XCheckBox {
            id: compensate
            enabled: !downloading
            visible: !downloading
            text: qsTr("Compensate Pitch Drift")
            height: vsize
            x: delayed.x
            width: delayed.width
            fontsize: simple.fontsize
            property bool active: driftCorrectionParameter > 0.001
            checked: active
            onActiveChanged: checked = active
            onClicked: {
                if (driftCorrectionParameter < 0.001) driftCorrectionParameter = 0.5;
                else driftCorrectionParameter = 0;
            }
        }
    }

    // Download indicator
    Rectangle {
        anchors.top: middleArea.top
        anchors.topMargin: middleArea.height * 0.4
        anchors.left: middleArea.left
        anchors.right: middleArea.right
        anchors.leftMargin: middleArea.width*0.1
        anchors.rightMargin: middleArea.width*0.1
        height: vsize*2.1
        color: "black"
        radius: vsize*0.25
        border.color: "gray"
        border.width: vsize*0.05
        visible: downloading


        Gauge {
            id: gauge
            anchors.fill: parent
            anchors.leftMargin: vsize*0.15
            anchors.topMargin: vsize*0.2
            anchors.bottomMargin: vsize*0.1
            anchors.rightMargin: vsize*0.15

            orientation: Qt.Horizontal
            minimumValue: 0
            value: downloadingProgress
            maximumValue: 100
            font.pixelSize: fontsize*0.5
            font.family: window.font
            Behavior on value { NumberAnimation { duration: 300 }}

            style: GaugeStyle {

                minorTickmark: Item {
                    implicitWidth: fontsize*0.3
                    implicitHeight: 1

                    Rectangle {
                        color: "#cccccc"
                        anchors.fill: parent
                        anchors.leftMargin: fontsize*0.1
                        anchors.rightMargin: fontsize*0.1
                    }
                }

                tickmark: Item {
                    implicitWidth: fontsize*0.6
                    implicitHeight: 1

                    Rectangle {
                        color: "#c8c8c8"
                        anchors.fill: parent
                        anchors.leftMargin: fontsize*0.1
                        anchors.rightMargin: fontsize*0.1
                    }
                }

                valueBar: Rectangle {
                    implicitWidth: vsize*0.8
                    readonly property double fraction: gauge.value/ gauge.maximumValue
                    border.color: "gray"
                    color: Qt.rgba(fraction < 0.5 ? 1 : 2-2*fraction, 0.7*fraction, 0, 1)
                    radius: vsize*0.2
                }
            }
        }
        Text {
            text: qsTr("Downloading Instruments") + (downloadedInstrument > 0 ?
                  " (" + downloadedInstrument + "/" + totalNumberOfInstruments + ")" : "");
            horizontalAlignment: Text.AlignHCenter
            color: "white"
            font.family: window.font
            anchors.fill: parent
            anchors.topMargin: gauge.height * 0.1
            font.pixelSize: window.fontsize * 0.75
        }
    }

    //----------------------------- Footer Hint -------------------------------

    Rectangle {
        id: mask
        visible: window.showWelcome
        anchors.top: instrumentRow.top
        anchors.bottom: middleArea.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        color: "white"
        opacity: 0.8
    }

    Item {
        id: hint
        visible: window.showWelcome
        anchors.left: footer.left
        anchors.right: footer.right
        anchors.bottom: footer.top
        anchors.top: header.bottom
        anchors.leftMargin: footer.width/2-scalefactor*0.45
        anchors.bottomMargin: -parent.height*0.05

        Image {
            id: arrow
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            height: Math.min(footer.height,parent.width*0.3)*0.8
            fillMode: Image.PreserveAspectFit
            source: "icons/hint.png"
        }
        Rectangle {
            color: "#ffe0e0"
            anchors.left: arrow.right
            anchors.bottom: arrow.top
            width: parent.width*0.7
            height: parent.width*0.2
            radius: height*0.1
            border.width: 2
            border.color: "black"



            Text {
                anchors.fill: parent
                anchors.margins: parent.width*0.07
                text: qsTr("Tap this button to hear a demo");
                font.pixelSize: parent.height*0.25
                font.family: window.font
                font.bold: true
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
            }
        }
    }

    PropertyAnimation {
        id: animation1;
        target: hint;
        property: "opacity";
        from: 1
        to: 0
        duration: 5000
        onStopped: {
            hint.visible=false;
        }
    }

    PropertyAnimation {
        id: animation2;
        target: mask;
        property: "opacity";
        to: 0
        duration: 8000
        onStopped: {
            mask.visible=false;
        }
    }

    Timer {
        id: timer
        running: window.showWelcome
        interval: 5000
        repeat: false
        onTriggered: {
            animation1.start();
            animation2.start();
        }
    }

}
