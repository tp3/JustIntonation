/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

import QtQuick 2.3
import QtQuick.Controls 1.3
import QtQuick.Dialogs 1.0

Item {
    id: systemsettings
    property int fontsize: parent.width*0.06
    property int vsize: parent.width*0.1

    Column {
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.topMargin: fontsize*0.5
        anchors.leftMargin: fontsize*0.8
        anchors.rightMargin: fontsize*0.8
        spacing: parent.height*0.0535

        XCheckBox {
            id: welcomeMessageButton
            text: qsTr("Welcome screen on startup")
            height: vsize
            checked: window.showWelcome
            onCheckedChanged: window.showWelcome = checked
            fontsize: systemsettings.fontsize
        }

        XCheckBox {
            text: qsTr("Allow download of samples")
            height: vsize
            checked: window.allowDownload
            onCheckedChanged: window.allowDownload = checked
            fontsize: systemsettings.fontsize
        }

        Item {
            width: parent.width
            height: forcedDownload.height
            Text {
                id: text
                anchors.fill: parent
                text: qsTr("Language")+":";
                font.family: window.font
                verticalAlignment: Text.AlignVCenter
                font.pixelSize: fontsize
            }
            XCombo {
                id: languageSelector
                height: vsize
                anchors.fill: parent
                anchors.leftMargin: text.contentWidth*1.2
                fontsize: systemsettings.fontsize
                model: translator.languages
                currentIndex: translator.index
                onCurrentIndexChanged: if (count>0) translator.index=currentIndex
            }
        }

        XButton {
            id: forcedDownload
            text: qsTr("Forced instrument download")
            tooltip: ("Reload all instruments from the internet")
            fontsize: systemsettings.fontsize
            width: parent.width
            height: vsize
            onClicked: {
                stack.pop(null);
                setQmlSelectedInstrument(0);
                allowDownload = true;
                downloadedInstrument = 0;
                startDownload(true);
            }
        }

        Item {
            id: dummy
            width: parent.width
            height: vsize
        }

        XButton {
            id: resetSettings
            text: qsTr("Restore default settings")
            tooltip: qsTr("Reset all settings and restart the application")
            fontsize: systemsettings.fontsize
            width: parent.width
            height: vsize
            onClicked: stack.push(resetPrefDialog)
        }
    }
}
